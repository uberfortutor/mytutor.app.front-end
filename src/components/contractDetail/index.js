import React, { Component } from 'react';
import { get, isEmpty, filter, pick } from 'lodash';
import { connect } from 'react-redux';
import {
  Button,
  TextField,
  InputAdornment,
  Chip,
  Typography,
  Avatar
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import { getCookie } from '@supports/utils/cookies';
import queryString from 'query-string';
import { withRouter, Redirect } from 'react-router-dom';
import Loading from '@commons/components/loading';
import { restClient } from '../../supports/rest-client';

const useStyles = makeStyles({
  chipItem: {
    fontWeight: 'bold'
  },
  containerContract: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 30px',
    margin: '20px 0'
  },
  titleContract: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    paddingBottom: 10
  },
  customButton: {
    padding: '8px 0',
    display: 'flex',
    justifyContent: 'center'
  },
  roleIn: {
    margin: '5px 0'
  },
  spaceBottom: {
    marginBottom: '10px'
  },
  customContent: {
    padding: '10px 30px',
    marginBottom: '20px'
  },
  titleBody: {
    paddingBottom: 10
  },
  marginBottom5: {
    marginBottom: 5
  },
  price: {
    marginTop: 5
  },
  done: {
    marginTop: 5,
    padding: '15px 0'
  },
  waiting: { marginTop: 20 },
  lession: { marginTop: 5 },
  pricePerLession: { marginTop: 5 },
  namePeople: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  detailName: {
    fontSize: '18px',
    textTransform: 'uppercase',
    fontWeight: '600'
  },
  detailInfo: {
    display: 'flex',
    marginTop: 10
  },
  detailAvatar: {
    maxWidth: '120px'
  },
  avatarImage: {
    width: '100%',
    height: '100%'
  },
  detailProfile: {
    marginLeft: '20px',
    lineHeight: '1.5'
  }
});

const StatusComponent = ({ status }) => {
  const classes = useStyles();
  console.log('status', typeof status);

  switch (status) {
    case -1 || null || undefined: {
      return null;
    }
    case 0:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Đang thương lượng"
          style={{ border: '2px solid #FFC108', color: '#FFC108' }}
        />
      );
    case 1:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Đang dạy"
          style={{ border: '2px solid #128496', color: '#128496' }}
        />
      );
    case 2:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Hoàn tất hợp đồng"
          style={{ border: '2px solid #218838', color: '#218838' }}
        />
      );
    case 3:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Đã huỷ hợp đồng"
          style={{ border: '2px solid #C82333', color: '#C82333' }}
        />
      );
    case 10:
      return (
        <Chip
          className={classes.chipItem}
          variant="contained"
          label="Đã kí"
          style={{ background: '#218838', color: 'white' }}
        />
      );
    case 11:
      return (
        <Chip
          className={classes.chipItem}
          variant="contained"
          label="Chưa kí"
          style={{ background: '#C82333', color: 'white' }}
        />
      );
    default:
      return null;
  }
};

const ContractDetail = ({ action, update, role, match }) => {
  const status = -1;
  const AVATAR_DEFAUL = '/media/avatar-default.png';
  const classes = useStyles();
  const newDate = new Date();
  const currentDay =
    newDate.getFullYear() + '/' + newDate.getMonth() + '/' + newDate.getDate();
  const contractName = `Hợp đồng dạy số  ${newDate.getTime()}`;

  const { id } = match.params || {};

  const [contract, setContract] = React.useState(null);
  const [loadedContract, setLoadedContract] = React.useState(false);
  const [createdContract, setCreatedContract] = React.useState(null);
  const [isProcessing, setIsProcessing] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [redirectTo, setRedirectTo] = React.useState(null);

  const [teach, setTeach] = React.useState(status === -1 ? '' : '');
  const [teachError, setTeachError] = React.useState(null);
  const [finderTuition, setFinderTuition] = React.useState('0');
  const [finderTuitionError, setFinderTuitionError] = React.useState(null);
  const [tutorTuition, setTutorTuition] = React.useState('0');
  const [tutorTuitionError, setTutorTuitionError] = React.useState(null);
  const [numberOfSession, setNumberOfSession] = React.useState('');
  const [numberOfSessionError, setNumberOfSessionError] = React.useState(null);
  const [numberOfHourPerSession, setNumberOfHourPerSession] = React.useState(
    ''
  );
  const [
    numberOfHourPerSessionError,
    setNumberOfHourPerSessionError
  ] = React.useState(null);

  const handleChangeTeach = e => {
    setTeach(e.target.value);
  };

  const handleChangeFinderTuition = e => {
    setFinderTuition(e.target.value);
  };

  const handleChangeTutorTuition = e => {
    setTutorTuition(e.target.value);
  };

  const handleChangeNumberOfSession = e => {
    setNumberOfSession(e.target.value);
  };

  const handleChangeNumberOfHourPerSession = e => {
    setNumberOfHourPerSession(e.target.value);
  };

  const loadContract = () => {
    setLoading(true);

    restClient.asyncGet(`/contracts/me/${id}`).then(res => {
      setLoading(false);
      setContract(get(res, ['data', 'contract'], null));
    });
  };

  const validateCreateContract = () => {
    let valid = true;

    if (isEmpty(teach)) {
      setTeachError('Vui lòng không bỏ trống');
      valid = false;
    } else {
      setTeachError(null);
    }

    if (isEmpty(finderTuition)) {
      setFinderTuitionError('Vui lòng không bỏ trống');
      valid = false;
    } else {
      setFinderTuitionError(null);
    }

    return valid;
  };

  const validateUpdateContractForFinder = () => {
    let valid = true;

    if (isEmpty(teach)) {
      setTeachError('Vui lòng không bỏ trống');
      valid = false;
    } else {
      setTeachError(null);
    }

    if (isEmpty(finderTuition)) {
      setFinderTuitionError('Vui lòng không bỏ trống');
      valid = false;
    } else {
      setFinderTuitionError(null);
    }

    return valid;
  };

  const validateUpdateContractForTutor = () => {
    let valid = true;

    if (isEmpty(tutorTuition)) {
      setTutorTuitionError('Vui lòng không bỏ trống');
      valid = false;
    } else {
      setTutorTuitionError(null);
    }

    if (isEmpty(numberOfSession)) {
      setNumberOfSessionError('Vui lòng không bỏ trống');
      valid = false;
    } else {
      setNumberOfSessionError(null);
    }

    if (isEmpty(handleChangeNumberOfSession)) {
      setNumberOfHourPerSessionError('Vui lòng không bỏ trống');
      valid = false;
    } else {
      setNumberOfHourPerSessionError(null);
    }

    return valid;
  };

  const createContract = () => {
    if (!validateCreateContract()) {
      return;
    }

    setIsProcessing(true);

    restClient
      .asyncPost('/contracts/me', {
        name: contractName,
        teach,
        tuition: parseInt(finderTuition, 10) * 1000,
        tutorId: parseInt(id, 10)
      })
      .then(res => {
        setIsProcessing(false);
        const contractRes = get(res, ['data', 'contract']);
        setCreatedContract(contractRes);
      });
  };

  const updateContract = () => {
    if (
      !(role === 'tutor'
        ? validateUpdateContractForTutor()
        : validateUpdateContractForFinder())
    ) {
      return;
    }

    setIsProcessing(true);

    restClient
      .asyncPost(
        `/contracts/me/${id}/${
          role === 'finder' ? 'update-for-finder' : 'update-for-tutor'
        }`,
        role === 'tutor'
          ? {
              tuition: parseInt(tutorTuition, 10) * 1000,
              numberOfSession: parseInt(numberOfSession, 10),
              numberOfHourPerSession: parseInt(numberOfHourPerSession, 10)
            }
          : {
              tuition: parseInt(tutorTuition, 10) * 1000,
              teach
            }
      )
      .then(res => {
        const contractRes = get(res, ['data', 'contract']);
        setContract(contractRes);
        setIsProcessing(false);
        setRedirectTo('view');
      });
  };

  const signContract = () => {
    setIsProcessing(true);

    restClient.asyncPost(`/contracts/${id}/sign`).then(res => {
      setIsProcessing(false);
      setContract(get(res, ['data', 'contract']));
    });
  };

  const convertDate = dateString => {
    if (isEmpty(dateString)) {
      return '';
    }

    const date = new Date(dateString);

    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  };

  React.useEffect(() => {
    if (action !== 'new' && !loadedContract) {
      setLoadedContract(true);
      loadContract();
    }
  });

  React.useEffect(() => {
    console.log('inited');
    setTeach(get(contract, 'teach', '') || '');
    setTutorTuition(`${get(contract, 'confirmedTuition', 0) || 0}`);
    setFinderTuition(`${get(contract, 'tuitionOfFinder', 0) || 0}`);
    setNumberOfSession(`${get(contract, 'numberOfSession', 0) || 0}`);
    setNumberOfHourPerSession(
      `${get(contract, 'numberOfHourPerSession', 0) || 0}`
    );
  }, [contract]);

  return !isEmpty(createdContract) ? (
    <Redirect to={`/contracts/${createdContract.id}`} />
  ) : redirectTo === 'view' ? (
    <Redirect to={`/contracts/${get(contract, 'id')}`} />
  ) : redirectTo === 'update' ? (
    <Redirect to={`/contracts/${get(contract, 'id')}/update`} />
  ) : loading ? (
    <Loading size={40} />
  ) : (
    <>
      <Grid container style={{ justifyContent: 'center' }}>
        <Grid item xs={10} className={classes.spaceBottom}>
          <Card className={classes.containerContract}>
            <div>
              <span className={classes.titleContract}>
                {action === 'new'
                  ? contractName
                  : get(contract, 'name', 'Tên hợp đồng')}
              </span>
              <div className={classes.roleIn}>
                <div>
                  Ngày tạo:{' '}
                  {action === 'new'
                    ? currentDay
                    : convertDate(get(contract, 'createdDate'))}
                </div>
                {action === 'new' ? null : (
                  <div>
                    Cập nhật lần cuối:{' '}
                    {convertDate(get(contract, 'updatedDate', null) || null)}
                  </div>
                )}
              </div>
            </div>
            <div>
              <StatusComponent status={get(contract, 'status')} />
              {action === 'new' ? (
                <Button
                  variant="outlined"
                  color="primary"
                  disabled={isProcessing}
                  onClick={createContract}
                >
                  Tạo
                </Button>
              ) : action === 'view' ? (
                <>
                  {get(contract, 'status') === 0 ? (
                    <>
                      <Button
                        variant="outlined"
                        color="primary"
                        disabled={isProcessing}
                        style={{ margin: '0px 5px' }}
                        onClick={() => setRedirectTo('update')}
                      >
                        Cập nhật hợp đồng
                      </Button>
                      {get(
                        contract,
                        role == 'finder'
                          ? 'signRequestOfFinder'
                          : 'signRequestOfTutor'
                      ) ? null : (
                        <Button
                          variant="outlined"
                          color="primary"
                          disabled={isProcessing}
                          style={{ margin: '0px 5px' }}
                          onClick={signContract}
                        >
                          Kí
                        </Button>
                      )}
                    </>
                  ) : null}
                  {get(contract, 'status') < 2 ? (
                    <Button
                      variant="outlined"
                      color="primary"
                      disabled={isProcessing}
                      style={{ margin: '0px 5px' }}
                      // onClick={createContract}
                    >
                      Huỷ
                    </Button>
                  ) : null}
                </>
              ) : action === 'update' ? (
                <Button
                  size="small"
                  variant="contained"
                  color="primary"
                  disabled={isProcessing}
                  style={{ margin: '0px 5px' }}
                  onClick={updateContract}
                >
                  Cập nhật
                </Button>
              ) : null}
            </div>
          </Card>
          <Card className={classes.customContent}>
            <div className={classes.titleBody}>
              <span className={classes.titleContract}>Thông tin hợp đồng</span>
            </div>
            <div>
              <div className={classes.teaching}>
                {action === 'view' ? (
                  <span>
                    Các phần sẽ dạy:{' '}
                    <strong>{get(contract, 'teach') || ''}</strong>
                  </span>
                ) : (
                  <TextField
                    fullWidth
                    onChange={handleChangeTeach}
                    error={!isEmpty(teachError) || false}
                    label="Các phần sẽ dạy"
                    variant="standard"
                    multiline
                    id="teach"
                    name="teach"
                    type="text"
                    value={teach}
                  />
                )}
              </div>
              <div className={classes.price}>
                <div className={classes.done}>
                  Giá đã chốt:{' '}
                  <strong>
                    {action === 'new'
                      ? 'Chưa chốt'
                      : get(contract, 'confirmedTuition', 'Chưa chốt') ||
                        'Chưa chốt'}
                  </strong>
                </div>

                {(action === 'new' || action === 'update') &&
                role === 'finder' ? (
                  <TextField
                    onChange={handleChangeFinderTuition}
                    error={!isEmpty(finderTuitionError) || false}
                    //defaultValue={get(rViaSocial, ['info', 'email'], '')}
                    label="Giá đề nghị từ người học"
                    variant="standard"
                    id="finderTuition"
                    name="finderTuition"
                    type="number"
                    value={finderTuition}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment
                          position="end"
                          style={{ padding: '0 10px' }}
                        >
                          {' '}
                          k/h
                        </InputAdornment>
                      )
                    }}
                  />
                ) : (
                  <div className={classes.waiting}>
                    Giá đề nghị từ người học:
                    <strong>
                      {(get(contract, 'tuitionOfFinder', 0) || 0) / 1000} k/h
                    </strong>
                  </div>
                )}

                {action === 'update' && role === 'tutor' ? (
                  <TextField
                    onChange={handleChangeTutorTuition}
                    error={!isEmpty(tutorTuitionError) || false}
                    //defaultValue={get(rViaSocial, ['info', 'email'], '')}
                    label="Giá đề nghị từ người dạy"
                    variant="standard"
                    id="tutorTuition"
                    name="tutorTuition"
                    type="text"
                    value={tutorTuition}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment
                          position="end"
                          style={{ padding: '0 10px' }}
                        >
                          {' '}
                          k/h
                        </InputAdornment>
                      )
                    }}
                  />
                ) : (
                  <div className={classes.waiting}>
                    Giá đề nghị từ người dạy:{' '}
                    <strong>
                      {(get(contract, 'confirmedTuition', 0) || 0) / 1000} k/h
                    </strong>
                  </div>
                )}
              </div>
              {(action === 'new' || action === 'update') && role === 'tutor' ? (
                <>
                  <TextField
                    fullWidth
                    onChange={handleChangeNumberOfSession}
                    error={!isEmpty(numberOfSessionError)}
                    label="Số lượng buổi học"
                    variant="standard"
                    id="lession"
                    name="lession"
                    type="text"
                    value={numberOfSession}
                  />
                  <TextField
                    fullWidth
                    onChange={handleChangeNumberOfHourPerSession}
                    error={!isEmpty(numberOfHourPerSessionError)}
                    label="Thời lượng mỗi buổi học"
                    variant="standard"
                    id="timeOfLession"
                    name="timeOfLession"
                    type="text"
                    value={numberOfHourPerSession}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">/h</InputAdornment>
                      )
                    }}
                  />
                </>
              ) : (
                <div className={classes.lession}>
                  <div className={classes.amount}>
                    Số lượng buổi học:{' '}
                    <strong>{get(contract, 'numberOfSession', 0) || 0}</strong>
                  </div>
                  <div className={classes.pricePerLession}>
                    Thời lượng một buổi học:
                    <strong>
                      {get(contract, 'numberOfHourPerSession', 0) || 0}h
                    </strong>
                  </div>
                </div>
              )}
            </div>
          </Card>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Card className={classes.customContent}>
                <div className={classes.namePeople}>
                  <span className={classes.detailName}>Người dạy</span>
                  {action === 'new' ? null : (
                    <StatusComponent
                      status={get(contract, 'signRequestOfTutor') ? 10 : 0}
                    />
                  )}
                </div>
                <div className={classes.detailInfo}>
                  <div className={classes.detailAvatar}>
                    <Avatar
                      alt="Remy Sharp"
                      className={classes.avatarImage}
                      src={get(contract, ['tutor', 'avatar']) || AVATAR_DEFAUL}
                    />
                  </div>
                  <div className={classes.detailProfile}>
                    <div>
                      Tên:{' '}
                      {get(contract, ['tutor', 'displayName']) ||
                        'Tên người dạy'}
                    </div>
                    <div>
                      Email:{' '}
                      {get(contract, ['tutor', 'email']) || 'Email người dạy'}
                    </div>
                    <div>
                      Điện thoại:{' '}
                      {get(contract, ['tutor', 'phone']) ||
                        'Điện thoại của người dạy'}
                    </div>
                    <div>
                      Địa chỉ: {get(contract, ['tutor', 'address', 'detail'])},
                      {get(contract, ['tutor', 'address', 'ward'])},
                      {get(contract, ['tutor', 'address', 'province'])}
                    </div>
                  </div>
                </div>
              </Card>
            </Grid>
            <Grid item xs={6}>
              <Card className={classes.customContent}>
                <div className={classes.namePeople}>
                  <span className={classes.detailName}>Người học</span>
                  {action === 'new' ? null : (
                    <StatusComponent
                      status={get(contract, 'signRequestOfFinder') ? 10 : 0}
                    />
                  )}
                </div>
                <div className={classes.detailInfo}>
                  <div className={classes.detailAvatar}>
                    <Avatar
                      alt="Remy Sharp"
                      className={classes.avatarImage}
                      src={get(contract, ['finder', 'avatar']) || AVATAR_DEFAUL}
                    />
                  </div>
                  <div className={classes.detailProfile}>
                    <div>
                      Tên:{' '}
                      {get(contract, ['finder', 'displayName']) ||
                        'Tên người học'}
                    </div>
                    <div>
                      Email:{' '}
                      {get(contract, ['finder', 'email']) || 'Email người học'}
                    </div>
                    <div>
                      Điện thoại:{' '}
                      {get(contract, ['finder', 'phone']) ||
                        'Điện thoại của người học'}
                    </div>
                    <div>
                      Địa chỉ: {get(contract, ['finder', 'address', 'detail'])},
                      {get(contract, ['finder', 'address', 'ward'])},
                      {get(contract, ['finder', 'address', 'province'])}
                    </div>
                  </div>
                </div>
              </Card>
            </Grid>
          </Grid>
          {/* <CardActions className={classes.customButton}>
            {(action === 'update' && get(contract, 'status')) === 0 ? (
              <Button size="small" variant="contained" color="primary">
                Cập nhật
              </Button>
            ) : (
              ''
            )}
          </CardActions> */}
        </Grid>
      </Grid>
    </>
  );
};
export default withRouter(
  connect(
    state => ({
      role: get(state, ['authReducer', 'user', 'metadata', 'role'])
    }),
    null
  )(ContractDetail)
);
