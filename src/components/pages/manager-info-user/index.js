import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { withPageTitle } from '@supports/hoc';
import ChangePassword from './change-password';
import UserProfile from './view-profile';

const useStyles = makeStyles({
  avatar: {
    width: '80px',
    height: '80px',
    borderRadius: '50%',
    overflow: 'hidden',
    position: 'relative'
  },
  avatarImage: {
    width: '100%',
    height: '100%'
  },
  sidebar: {
    background: '#fff'
  },
  containerManager: {
    position: 'absolute',
    height: '100%',
    margin: 0
  }
});

const ManagerInfo = () => {
  const classes = useStyles();

  return (
    <Grid container className={classes.containerManager}>
      <Grid item xs={1} />
      <Grid item xs={10}>
        <Switch>
          <Route
            path="/me/info/profile"
            component={withPageTitle(UserProfile, 'Thông tin tài cá nhân')}
          />
          <Route
            path="/me/info/change-password"
            component={withPageTitle(ChangePassword, 'Đổi mật khẩu')}
          />
        </Switch>
      </Grid>
      <Grid item xs={1} />
    </Grid>
  );
};

export default ManagerInfo;
