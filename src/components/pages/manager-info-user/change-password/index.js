import React from 'react';
import {
  Container,
  Typography,
  Grid,
  TextField,
  Box,
  CircularProgress,
  Fade
} from '@material-ui/core';
import { withRouter } from 'react-router';
import Select from 'react-select';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { get, isEmpty } from 'lodash';
import { H1, Link, Button } from '@commons/components';
import { restClient } from '@supports/rest-client';
import WarningIcon from '@material-ui/icons/Warning';
import FlatErrorMessage from '../../../../commons/components/flat-error-message';

const useStyles = makeStyles({
  containerBody: {
    position: 'relative',
    margin: '60px auto',
    padding: 20,
    background: '#fff',
    boxShadow: '2px 4px 20px 0 rgba(0,0,0,0.2)'
  },
  errNoti: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'red',
    textAlign: 'center',
    marginBottom: '20px'
  },

  showLoading: {
    padding: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',

    display: 'none',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingComponent: {
    textAlign: 'center',
    color: '#0677BC'
  }
});

const ChangePassword = () => {
  const classes = useStyles();
  const [oldPassword, setOldPassword] = React.useState('');
  const [newPassword, setNewPassword] = React.useState('');
  const [oldPasswordError, setOldPasswordError] = React.useState(null);
  const [newPasswordError, setNewPasswordError] = React.useState(null);
  const [isProcessing, setIsProcessing] = React.useState(false);
  const [messageFail, setMessageFail] = React.useState('');

  const changePassword = () => {
    setIsProcessing(true);

    restClient
      .asyncPost('/auth/me/change-password', { oldPassword, newPassword })
      .then(res => {
        console.log('res', res);
        if (get(res, 'data')) {
          if (get(res, ['data', 'status'])) {
            // notify change password success
            setOldPassword('');
            setNewPassword('');
          } else {
            setMessageFail(get(res, ['data', 'message']));
          }
        }

        setIsProcessing(false);
      })
      .catch(err => {
        console.log('error: ', err.response);
        const { status, data } = get(err, 'response', {});
      });
  };

  const handleChangeOldPassword = e => {
    setOldPassword(e.target.value);
  };

  const handleChangeNewPassword = e => {
    setNewPassword(e.target.value);
  };

  const validateInfo = () => {
    let validToSubmit = true;

    setMessageFail('');

    if (isEmpty(oldPassword)) {
      setOldPasswordError('Vui lòng nhập mật khẩu cũ của bạn.');
      validToSubmit = false;
    } else {
      setOldPasswordError(null);
    }

    if (isEmpty(newPassword)) {
      setNewPasswordError('Vui lòng nhập mật khẩu cũ của bạn.');
      validToSubmit = false;
    } else {
      setNewPasswordError(null);
    }

    return validToSubmit;
  };

  const handleSubmit = e => {
    e.preventDefault();

    if (!validateInfo()) {
      return;
    }

    changePassword();
  };

  return (
    <Container maxWidth="sm" className={classes.containerBody}>
      <H1 style={{ margin: '20px 0', textTransform: 'uppercase' }}>
        ĐỔI MẬT KHẨU
      </H1>
      <form onSubmit={handleSubmit}>
        <FlatErrorMessage message={messageFail} />
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              error={oldPasswordError || false}
              fullWidth
              onChange={handleChangeOldPassword}
              label="Mật khẩu cũ"
              type="password"
              variant="outlined"
              id="oldPassword"
              name="oldPassword"
              value={oldPassword}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              onChange={handleChangeNewPassword}
              error={newPasswordError || false}
              label="Mật khẩu mới"
              variant="outlined"
              id="newPassword"
              name="newPassword"
              type="password"
              value={newPassword}
            />
          </Grid>

          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              disabled={isProcessing}
              type="submit"
            >
              GỬI
            </Button>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
};
export default connect(null, null)(ChangePassword);
