import React from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import GeneralProfile from './general';
import TutorInfo from './tutor';

const useStyles = makeStyles({
  
  tabList: {
    marginBottom: 0
  },
  title: {
    fontSize: '24px',
    color: '#0677BC',
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
    marginTop: '20px'
  }
});

const UserProfile = ({ role }) => {
  const classes = useStyles();
  const [tab, setTab] = React.useState('profile');
  console.log('tab', tab);

  React.useEffect(() => {
    console.log('kkkkkkkkkk', role);
  });

  return (
    <>
      <Typography className={classes.title}>
        Cập nhật đầy đủ thông tin cá nhân của bạn
      </Typography>
      <Tabs value={tab}>
        <TabList className={classes.tabList}>
          <Tab value="profile" onClick={() => setTab('profile')}>
            Thông tin cá nhân
          </Tab>
          {role === 'tutor' ? (
            <Tab value="tutorInfo" onClick={() => setTab('tutorInfo')}>
              Thông tin gia sư
            </Tab>
          ) : null}
        </TabList>

        <TabPanel style={{ position: 'relative' }} >
          <GeneralProfile />
        </TabPanel>
        {role === 'tutor' ? (
          <TabPanel style={{ position: 'relative' }}>
            <TutorInfo />
          </TabPanel>
        ) : (
          ''
        )}
      </Tabs>
    </>
  );
};

const mapStateToProps = state => ({
  role: get(state, ['authReducer', 'user', 'metadata', 'role'])
});

export default connect(mapStateToProps, null)(UserProfile);
