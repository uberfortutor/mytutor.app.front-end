import React, { Component } from 'react';
import { get, isEmpty, filter } from 'lodash';
import { connect } from 'react-redux';
import Loading from '@components/loading';
import {
  Typography,
  Container,
  Chip,
  Grid,
  Button,
  Input,
  TextField,
  InputLabel,
  Box,
  MenuItem,
  CircularProgress
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import Select from 'react-select';
import {
  updateProfileTutor,
  getTutorInfo,
  getSkills,
  getSkillSuccess
} from '@supports/store/redux/actions';
import { bindActionCreators } from 'redux';
import WarningIcon from '@material-ui/icons/Warning';
import './tutor.css';
import { restClient } from '../../../../supports/rest-client';

const useStyles = makeStyles({
  avatar: {},
  header__info: {},
  general: {
    background: 'white',
    borderTop: '1px solid #aaa',
    padding: '30px 0'
  },
  marginTop10: { marginTop: '15px' },
  errMessage: {
    display: 'flex',
    color: 'red',
    justifyContent: 'center',
    marginBottom: '10px'
  },
  skillInput: {
    '& label': {
      color: 'red'
    }
  }
});

const TutorInfo = ({
  tutorInfo,
  actions,
  skills,
  isGettingSkill,
  isGettingTutorInfo,
  isUpdatingProfile
}) => {
  const classes = useStyles();

  const [initedValues, setInitedValues] = React.useState(false);
  const [job, setJob] = React.useState(
    get(tutorInfo, ['tutor', 'job'], '') || ''
  );
  const [description, setDescription] = React.useState(
    get(tutorInfo, ['tutor', 'description'], '') || ''
  );
  const [tuitionPerHour, setTuitionPerHour] = React.useState(
    get(tutorInfo, ['tutor', 'tuitionPerHour'], 0) || 0
  );

  const [skill, setSkill] = React.useState(get(tutorInfo, 'skills', []) || []);

  const [messageFail, setMessageFail] = React.useState(false);
  const [inValidPrice, setInValidPrice] = React.useState(false);
  const [isCreatingSkill, setIsCreatingSkill] = React.useState(false);
  const [backupSkills, setBackupSkills] = React.useState(
    Object.assign([], skills || [])
  );

  const handleChangeJob = e => {
    setJob(e.target.value);
  };
  const handleChangeDescription = e => {
    setDescription(e.target.value);
  };
  const handleChangeTuitionPerHour = e => {
    const valueInput = e.target.value;
    console.log('test', valueInput);
    if (Number.isInteger(+valueInput) && valueInput.length < 5) {
      setInValidPrice(false);
      setTuitionPerHour(valueInput);
    } else return setInValidPrice(true);
  };

  const handleChangeSkill = (event, val) => {
    console.log('val', val);
    console.log('skill', skill);
    const test = [];
    val.forEach(i => {
      const a = filter(backupSkills, r => {
        return r.title === i;
      });
      test.push(a[0]);
    });
    console.log(test);
    setSkill(test);
  };

  const handleSumbit = e => {
    e.preventDefault();
    e.stopPropagation();
    setMessageFail(false);
    const filteredSkill = skill.filter(skl => skl.id);

    if (
      isEmpty(filteredSkill) ||
      isEmpty(job) ||
      isEmpty(description) ||
      isEmpty(tuitionPerHour)
    ) {
      setMessageFail(true);
      return;
    }

    const data = {
      job,
      description,
      tuitionPerHour: parseInt(tuitionPerHour, 10) * 1000,
      skills: filteredSkill.map(skl => skl.id)
    };

    actions.updateProfileTutor(data);
  };

  if (!isGettingSkill && isEmpty(skills)) {
    actions.getSkills();
  }

  if (!isGettingTutorInfo && isEmpty(tutorInfo)) {
    actions.getTutorInfo();
  }

  React.useEffect(() => {
    setBackupSkills(Object.assign([], skills || []));
  }, [skills]);

  React.useEffect(() => {
    console.log('backup skills', backupSkills);
    setSkill(
      backupSkills.filter(
        bSkl =>
          skill.findIndex(
            skl => skl.id === bSkl.id || skl.title === bSkl.title
          ) > -1
      )
    );
  }, [backupSkills]);

  React.useEffect(() => {
    if (!isGettingTutorInfo && !isEmpty(tutorInfo) && !initedValues) {
      console.log('init');
      setInitedValues(true);
      setJob(get(tutorInfo, ['tutor', 'job'], '') || '');
      setTuitionPerHour(
        `${(get(tutorInfo, ['tutor', 'tuitionPerHour'], 0) || 0) / 1000}`
      );
      setDescription(get(tutorInfo, ['tutor', 'description'], '') || '');
      setSkill(get(tutorInfo, ['skills'], []) || []);
    }
  });

  return isGettingSkill || isGettingTutorInfo ? (
    <Loading />
  ) : (
    <Container className={classes.general}>
      {messageFail ? (
        <div className={classes.errMessage}>
          <WarningIcon />
          <Typography>
            Thông tin của bạn nhập không hợp lệ. Mời kiểm tra lại thông tin !
          </Typography>
        </div>
      ) : (
        ''
      )}
      <form style={{ margin: 0, padding: 0 }}>
        <Grid container style={{ justifyContent: 'center' }}>
          <Grid item xs={8}>
            <TextField
              fullWidth
              error={isEmpty(job)}
              label="Nghề nghiệp"
              onChange={handleChangeJob}
              defaultValue={job}
            />
          </Grid>
          <Grid item xs={8} className={classes.marginTop10}>
            <TextField
              fullWidth
              error={isEmpty(tuitionPerHour) || inValidPrice}
              label="Giá dạy trong 1 giờ (x1000 VNĐ)"
              onChange={handleChangeTuitionPerHour}
              helperText={
                inValidPrice
                  ? 'Giá trị chỉ là số và có chiều dài tối đa là 4 kí tự'
                  : ''
              }
              inputProps={{
                maxLength: 4
              }}
              defaultValue={tuitionPerHour}
            />
          </Grid>
          <Grid item xs={8} className={classes.marginTop10}>
            <Autocomplete
              multiple
              // disabled={isCreatingSkill}
              // open={!isCreatingSkill}
              loading={true}
              error={isEmpty(skill)}
              id="skills"
              options={skills.map(option => option.title)}
              onChange={handleChangeSkill}
              filterSelectedOptions
              renderTags={(value, getTagProps) => {
                return value.map((option, index) =>
                  skill.findIndex(skl => !skl.id && skl.title === option) >
                  -1 ? (
                    <Chip
                      variant="outlined"
                      label={option}
                      {...getTagProps({ index })}
                      deleteIcon={<CircularProgress size={10} />}
                    />
                  ) : (
                    <Chip
                      variant="outlined"
                      label={option}
                      {...getTagProps({ index })}
                      // onDelete={() => }
                    />
                  )
                );
              }}
              value={skill.map(skl => skl.title)}
              renderInput={params => (
                <div style={{ position: 'relative' }}>
                  <TextField
                    {...params}
                    variant="standard"
                    error={isEmpty(skill)}
                    label="Kỹ năng và kinh nghiệm của bạn về"
                    placeholder="Kỹ năng của bạn"
                    fullWidth
                    onKeyUp={e => {
                      if (e.key === 'Enter' && e.target.value !== '') {
                        if (
                          backupSkills.some(skl => skl.title === e.target.value)
                        ) {
                          handleChangeSkill(null, [
                            ...skill.map(skl => skl.title),
                            e.target.value
                          ]);
                          e.target.value = '';
                          return;
                        }

                        setIsCreatingSkill(true);
                        setSkill([...skill, { title: e.target.value }]);
                        restClient
                          .asyncPost('/skills', {
                            title: e.target.value
                          })
                          .then(res => {
                            const data = get(res, 'data', {}) || {};
                            console.log('data', data);
                            if (data.status) {
                              actions.getSkillSuccess([...skills, data.skill]);
                            }

                            setIsCreatingSkill(false);
                          });
                      }
                    }}
                  />
                </div>
              )}
            />
          </Grid>
          <Grid item xs={8} className={classes.marginTop10}>
            <TextField
              fullWidth
              variant="outlined"
              error={isEmpty(description)}
              multiline
              rows="4"
              label="Mô tả về bản thân và kinh nghiệm của bạn"
              onChange={handleChangeDescription}
              defaultValue={description}
            />
          </Grid>
        </Grid>
        <Button
          variant="outlined"
          color="primary"
          style={{ margin: 'auto', display: 'flex', marginTop: '20px' }}
          type="button"
          onClick={handleSumbit}
          disabled={isUpdatingProfile}
        >
          Cập nhật
        </Button>
      </form>
    </Container>
  );
};

const mapStateToProps = state => ({
  tutorInfo: get(state, ['authReducer', 'user', 'tutor']),
  isGettingSkill: get(state, ['mainReducer', 'isGettingSkill']),
  isUpdatingProfile: get(state, ['authReducer', 'isUpdatingProfile']),
  isGettingTutorInfo: get(state, ['authReducer', 'isGettingTutorInfo']),
  skills: get(state, ['mainReducer', 'skills'], [])
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      updateProfileTutor,
      getTutorInfo,
      getSkills,
      getSkillSuccess
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(TutorInfo);
