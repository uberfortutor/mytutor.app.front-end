import React, { Component } from 'react';
import { get, isEmpty } from 'lodash';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { connect } from 'react-redux';
import { spacing } from '@material-ui/system';
import Alert from '@components/alert'
import Loading from '@components/loading';
import { useInput } from '@supports/utils/hookUseInput';
import {
  Typography,
  Container,
  Avatar,
  Grid,
  Button,
  TextField,
  InputLabel,
  Select,
  Box,
  CardActions
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import PublishIcon from '@material-ui/icons/Publish';
import InputAdornment from '@material-ui/core/InputAdornment';
import dataAddress from '@globals/dataAddress';
import {
  uploadAvatar,
  updateProfile,
  getProfile
} from '@supports/store/redux/actions';
import WarningIcon from '@material-ui/icons/Warning';

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from '@material-ui/pickers';
import { bindActionCreators } from 'redux';

const useStyles = makeStyles({
  header__info: {
    padding:'0 10px'
  },
  marginTop5: {
    marginTop: '10px'
  },
  buttonAvatar: {
    margin: '15px 0'
  },
  nameInfo: {
    textAlign: 'center',
    fontSize: '20px',
    fontWeight: 'bold'
  },
  emailInfo: {
    color: '#aaa',
    textAlign: 'center',
    marginTop: '10px',
    fontStyle: 'italic'
  },
  avatar: {
    width: '120px',
    height: '120px',
    borderRadius: '50%',
    overflow: 'hidden',
    position: 'relative',
    margin: 'auto',
    background: '#fff'
  },
  avatarImage: {
    width: '100%',
    height: '100%'
  },
  infoUpdate: {
    padding: '0 20px'
  },
  updateButton: {
    textAlign: 'center',
    marginTop: '20px'
  },
  errMessage: {
    display: 'flex',
    color: 'red',
    justifyContent: 'center',
    marginBottom: '10px'
  },
  general: {
    background: 'white',
    borderTop: '1px solid #aaa',
    padding: '30px 0'
  },
});

const AVATAR_DEFAUL = '/media/avatar-default.png';

const GeneralProfile = ({
  profile,
  isGettingProfile,
  actions,
  isUpdatingAvatar,
  isUpdatingProfile
}) => {
  const classes = useStyles();

  const [gender, setGender] = React.useState(get(profile, 'gender') || '');
  const [birthday, setBirthday] = React.useState(
    get(profile, 'birthday') || new Date()
  );
  const [city, setCity] = React.useState(
    get(profile, ['address', 'province']) || ''
  );
  const [district, setDistrict] = React.useState(
    get(profile, ['address', 'ward']) || ''
  );
  const [address, setAddress] = React.useState(
    get(profile, ['address', 'detail']) || ''
  );

  const [displayName, setDisplayName] = React.useState(
    get(profile, 'displayName') || ''
  );
  const [email, setEmail] = React.useState(get(profile, 'email') || '');
  const [phone, setPhone] = React.useState(get(profile, 'phone') || '');

  const [messageFail, setMessageFail] = React.useState(false);
  const [checkBirthday, setCheckBirthDay] = React.useState(false);
  const [inValidEmail, setInvalidEmail] = React.useState(false);
  const [isValidPhone, setIsValidPhone] = React.useState(false);

  const [isProcessUpdate,setIsProcessUpdate] = React.useState(false);
  // get value if change
  const handleChangeDisplayName = e => {
    setDisplayName(e.target.value);
  };
  const handleChangeEmail = e => {
    setEmail(e.target.value);
  };
  const handleChangePhone = e => {
    setPhone(e.target.value);
  };
  const handleChangeAvatar = e => {
    setAvatar(e.target.value);
  };
  const handleChangeSelectCity = e => {
    setCity(e.target.value);
  };
  const handleChangeSelectDistrict = e => {
    setDistrict(e.target.value);
  };
  const handleChangeAddress = e => {
    setAddress(e.target.value);
  };
  const handleChangeBirthday = (d, val) => {
    // console.log(console.log(d))
    setBirthday(val);
  };
  const handleChangeGender = e => {
    setGender(e.target.value);
  };

  // handle get new value
  const handleChangeInputImage = e => {
    const fileUpload = e.target.files[0];
    const formData = new FormData();
    formData.append('avatar', fileUpload);
    actions.uploadAvatar(formData);
  };
  const validateEmail = email => {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(String(email).toLowerCase());
  };
  const validatePhone = phone => {
    var re = /((09|03|07|08|05)+([0-9]{8})\b)/;
    return re.test(phone);
  };
  const isValidBirthday = birthday => {
    const current = new Date();
    const ofUser = new Date(birthday);
    console.log(current.getDate());
    console.log(ofUser.getDate());
    if (
      current.getDate() === ofUser.getDate() &&
      current.getMonth() === ofUser.getMonth() &&
      current.getFullYear() === ofUser.getFullYear()
    )
      return true;

    return false;
  };

  const handleSumbit = e => {
    e.preventDefault();

    const hasFieldEmpty =
      isEmpty(displayName) ||
      isEmpty(email) ||
      isEmpty(phone) ||
      isEmpty(gender) ||
      isEmpty(birthday) ||
      isEmpty(district) ||
      isEmpty(city) ||
      isEmpty(address);
    if (hasFieldEmpty) {
      console.log('...empty');
      setMessageFail(true);
    } else if (!validateEmail(email)) {
      console.log('..email');
      setInvalidEmail(!validateEmail(email));
    } else if (isValidBirthday(birthday)) {
      console.log('...birthday');
      setCheckBirthDay(!checkBirthday);
    } else if (!validatePhone(phone)) {
      console.log('...phone');
      setIsValidPhone(!validatePhone(phone));
    } else {
      setMessageFail(false);
      const data = {
        displayName,
        email,
        phone,
        gender,
        birthday,
        province: city,
        ward: district,
        detail: address
      };
      console.log(data);
      setIsProcessUpdate(!isProcessUpdate)
      actions.updateProfile(data);
    }
  };
  const districts = get(dataAddress[city], 'districts');

  console.log('ggggggg', isGettingProfile);
  if (!isGettingProfile && isEmpty(profile)) {
    actions.getProfile();
  }

  return isGettingProfile ? (
    <Loading />
  ) : (
    <Container className={classes.general}>
      <Grid container>
        <Grid item xs={3}>
          <div className={classes.header__info}>
            <div className={classes.avatar}>
              {isUpdatingAvatar ? (
                <Loading />
              ) : (
                <Avatar
                  alt="Remy Sharp"
                  className={classes.avatarImage}
                  src={get(profile, 'avatar') || AVATAR_DEFAUL}
                />
              )}
            </div>
            <label htmlFor="raised-button-file">
              <input
                accept="image/*"
                className={classes.input}
                style={{ display: 'none' }}
                id="raised-button-file"
                onChange={handleChangeInputImage}
                type="file"
              />
              <Button
                variant="outlined"
                fullWidth
                color="primary"
                component="span"
                className={classes.buttonAvatar}
              >
                <PublishIcon className={classes.uploadIcon} /> Cập nhật ảnh đại
                diện
              </Button>
            </label>
            <div className={classes.nameInfo}>{displayName}</div>
            <div className={classes.emailInfo}>{email}</div>
          </div>
        </Grid>
        <Grid item xs={9} className={classes.infoUpdate}>
          {messageFail ? (
            <div className={classes.errMessage}>
              <WarningIcon />
              <Typography>
                Thông tin của bạn nhập không hợp lệ. Mời kiểm tra lại thông tin
                !
              </Typography>
            </div>
          ) : (
            ''
          )}
          <form >
            <Grid container>
              <Grid item xs={7}>
                <TextField
                  fullWidth
                  error={isEmpty(displayName)}
                  label="Họ và tên"
                  defaultValue={displayName}
                  onChange={handleChangeDisplayName}
                />
              </Grid>
              <Grid item xs={1} />
              <Grid item xs={4}>
                <InputLabel htmlFor="gender">Giới tính</InputLabel>
                <Select
                  native
                  fullWidth
                  error={isEmpty(gender)}
                  value={gender}
                  onChange={handleChangeGender}
                  inputProps={{
                    name: 'gender',
                    id: 'gender'
                  }}
                >
                  <option value="female">Nữ</option>
                  <option value="male">Nam</option>
                  <option value="other">Khác</option>
                </Select>
              </Grid>
            </Grid>

            <Grid container className={classes.marginTop5}>
              <Grid item xs={7}>
                <TextField
                  label="Email"
                  fullWidth
                  error={inValidEmail || isEmpty(email)}
                  helperText={inValidEmail ? 'Email không đúng định dạng' : ''}
                  defaultValue={email}
                  onChange={handleChangeEmail}
                />
              </Grid>
            </Grid>
            <Grid container className={classes.marginTop5}>
              <Grid item xs={7}>
                <TextField
                  label="Số điện thoại"
                  fullWidth
                  error={isValidPhone || isEmpty(phone)}
                  helperText={
                    isValidPhone
                      ? 'Số điện thoại chỉ 10 chữ số và đúng định dạng. Mời kiểm tra lại'
                      : ''
                  }
                  defaultValue={phone}
                  onChange={handleChangePhone}
                />
              </Grid>
              <Grid item xs={1} />
              <Grid item xs={4}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    variant="inline"
                    format="yyyy-MM-dd"
                    invalidDateMessage="Ngày sinh không đúng định dạng. Mời chọn lại"
                    autoOk="true"
                    error={checkBirthday || isValidBirthday(birthday)}
                    helperText={
                      checkBirthday ? 'Mời chọn đúng ngày sinh của bạn' : ''
                    }
                    label="Ngày Sinh"
                    value={birthday}
                    onChange={handleChangeBirthday}
                    KeyboardButtonProps={{
                      'aria-label': 'change date'
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
            </Grid>
            <Grid container spacing={2} className={classes.marginTop5}>
              <Grid item xs={4}>
                <InputLabel htmlFor="city" error={isEmpty(city)}>
                  Tỉnh/Thành phố
                </InputLabel>
                <Select
                  native
                  fullWidth
                  error={isEmpty(city)}
                  value={city}
                  onChange={handleChangeSelectCity}
                  inputProps={{
                    name: 'city',
                    id: 'city'
                  }}
                >
                  {Object.keys(dataAddress).map(i => {
                    return <option value={i}>{dataAddress[i].name}</option>;
                  })}
                </Select>
              </Grid>
              <Grid item xs={4}>
                <InputLabel htmlFor="district" error={isEmpty(district)}>
                  Quận/Huyện
                </InputLabel>
                <Select
                  native
                  fullWidth
                  error={isEmpty(district)}
                  value={district}
                  onChange={handleChangeSelectDistrict}
                  inputProps={{
                    name: 'district',
                    id: 'district'
                  }}
                >
                  {districts
                    ? Object.keys(districts).map(function(key) {
                        return <option value={key}>{districts[key]}</option>;
                      })
                    : ''}
                </Select>
              </Grid>
              <Grid item xs={4}>
                <TextField
                  fullWidth
                  label="Địa chỉ"
                  defaultValue={address}
                  error={isEmpty(address)}
                  onChange={handleChangeAddress}
                />
              </Grid>
            </Grid>
            <Box className={classes.updateButton}>
              <Button variant="outlined" color="primary" onClick={handleSumbit} disabled={isUpdatingProfile}>
                Cập Nhật
              </Button>
            </Box>
            
          </form>
        </Grid>
      </Grid>
    </Container>
  );
};

const mapStateToProps = state => ({
  profile: get(state, ['authReducer', 'user', 'profile']),
  isGettingProfile: get(state, ['authReducer', 'isGettingProfile']),
  isUpdatingAvatar: get(state, ['authReducer', 'isUpdatingAvatar']),
  isUpdatingProfile:get(state, ['authReducer', 'isUpdatingProfile']),
  failedAuth: get(state, ['authReducer', 'failedAuth'])
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      uploadAvatar,
      updateProfile,
      getProfile
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(GeneralProfile);
