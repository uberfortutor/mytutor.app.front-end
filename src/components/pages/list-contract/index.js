import React from 'react';
import { get, isEmpty } from 'lodash';
import { withRouter } from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroll-component';
import ContractItem from '@components/contractItem';
import { Typography, Grid } from '@material-ui/core';
import { restClient } from '@supports/rest-client';
import Loading from '@commons/components/loading';
import Empty from '@commons/components/empty';
import { Fade } from 'react-reveal';

const ListContract = ({ match }) => {
  const id = get(match, ['params', 'id']);
  const [loaded, setloaded] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [contracts, setContracts] = React.useState([]);
  const [page, setPage] = React.useState(1);
  const [hasMore, setHasMore] = React.useState(true);

  React.useEffect(() => {
    if (!loaded) {
      setloaded(true);
      loadContracts();
    }
  });

  const loadContracts = () => {
    if (!hasMore) {
      return;
    }

    setHasMore(false);
    setLoading(true);

    restClient
      .asyncGet('/contracts/me', { page, pageSize: 12 })
      .then(res => {
        setLoading(false);
        const contractsRes = get(res, ['data', 'contracts']);
        console.log('contracts res', contractsRes);
        if (contractsRes.length === 0) {
          return;
        }

        setContracts([...contracts, ...contractsRes]);
        setPage(page + 1);
        setHasMore(true);
      })
      .catch(err => {});
  };

  return (
    <>
      <Typography
        component="div"
        style={{
          fontSize: '30px',
          textTransform: 'uppercase',
          color: '#0677BC',
          textAlign: 'center',
          padding: 20,
          fontWeight: 'bold'
        }}
      >
        Danh sách hợp đồng
      </Typography>
      <InfiniteScroll
        dataLength={contracts.length} //This is important field to render the next data
        next={loadContracts}
        hasMore={hasMore}
        loader={<Loading size={40} />}
        scrollableTarget="layoutContainer"
        style={{ overflow: 'hidden' }}
      >
        <Fade style={{ width: '100%!important' }}>
          <Grid container style={{ justifyContent: 'center' }}>
            {contracts.map(contract => (
              <ContractItem contract={contract} />
            ))}
          </Grid>
        </Fade>
      </InfiniteScroll>

      {loading ? <Loading size={40} /> : isEmpty(contracts) ? <Empty /> : null}
    </>
  );
};

export default withRouter(ListContract);
