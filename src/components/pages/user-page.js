import React from 'react';
import Loadable from 'react-loadable';
import { withRouter } from 'react-router';
import { get } from 'lodash';
import { Switch, Route } from 'react-router-dom';
import { shouldAuth, withPageTitle } from '@supports/hoc';
import ContractDetail from '@components/contractDetail';
import UserLayout from '../../containers/layouts/user-layout';
import HomePage from './home-page';
import ListTutor from './list-tutor';
import DetailTutorC from './detail-tutor';
import ListContract from './list-contract';
import Statistic from './statistic';
import SendResetPasswordRequest from './send-reset-password-request';
import ResetPassword from './reset-password';
// import ManagerInfo from './manager-info-user';

const ManagerInfo = Loadable({
  loader: () => import('./manager-info-user'),
  loading: 'loading'
});

function UserPage({ location }) {
  // console.log('user page');
  const UpdateContract = shouldAuth(
    withPageTitle(ContractDetail, 'Cập nhật hợp đồng')
  );

  const DetailContract = shouldAuth(
    withPageTitle(ContractDetail, 'Chi tiết hợp đồng')
  );

  const NewContract = shouldAuth(
    withPageTitle(ContractDetail, 'Đăng ký yêu cầu dạy')
  );

  return (
    <UserLayout>
      <Switch>
        <Route
          path="/"
          exact
          component={withPageTitle(HomePage, 'Kết nối gia sư trực tuyến')}
        />
        <Route
          path="/tutors"
          exact
          component={withPageTitle(ListTutor, 'Danh sách gia sư')}
        />
        <Route path="/tutors/:id/new-contract" exact>
          <NewContract action="new" />
        </Route>
        <Route
          path="/contracts"
          exact
          component={shouldAuth(
            withPageTitle(ListContract, 'Hợp đồng của tôi')
          )}
        />
        <Route path="/contracts/:id/update" exact>
          <UpdateContract action="update" update={true} />
        </Route>
        <Route path="/contracts/:id" exact>
          <DetailContract action="view" update={false} />
        </Route>
        <Route path="/tutors/:id" exact component={DetailTutorC} />
        <Route
          path="/me/info"
          component={shouldAuth(ManagerInfo, get(location, 'pathname'))}
        />
        <Route
          path="/statistic"
          exact
          component={withPageTitle(Statistic, 'Thống kê thu nhập')}
        />
        <Route
          path="/send-reset-password-request"
          exact
          component={withPageTitle(
            SendResetPasswordRequest,
            'Gửi yêu cầu khôi phục mật khẩu'
          )}
        />
        <Route
          path="/reset-password"
          exact
          component={withPageTitle(
            ResetPassword,
            'Gửi yêu cầu khôi phục mật khẩu'
          )}
        />
      </Switch>
    </UserLayout>
  );
}

export default withRouter(UserPage);
