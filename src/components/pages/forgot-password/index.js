import React from 'react';
import {
  Container,
  Typography,
  Grid,
  TextField,
  Box,
  CircularProgress,
  Fade
} from '@material-ui/core';
import { withRouter } from 'react-router';
import Select from 'react-select';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { get, isEmpty } from 'lodash';
import {
  H1,
  Link,
  Button
} from '@commons/components';

import WarningIcon from '@material-ui/icons/Warning';

const useStyles = makeStyles({
  containerBody: {
    position: 'relative',
    margin: '60px auto',
    padding: 20,
    background: '#fff',
    boxShadow: '2px 4px 20px 0 rgba(0,0,0,0.2)'
  },
  errNoti: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'red',
    textAlign: 'center',
    marginBottom: '20px'
  },
  
  showLoading: {
    padding: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',

    display: 'none',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingComponent: {
    textAlign: 'center',
    color: '#0677BC'
  },
  
});

const ForgotPassword = () => {
  const classes = useStyles();
  const [username, setUsername] = React.useState('');
  const [email, setEmail] = React.useState('');

  
  const [messageFail, setMessageFail] = React.useState('');

  const handleChangeUsername = e => {
    
    setUsername(e.target.value);
  };
  const handleChangeEmail = e => {
    
    setEmail(e.target.value);
  };
  
  const handleSubmit = e => {
    e.preventDefault();

    
  };

  

  return (
    
      <Container maxWidth="sm" className={classes.containerBody}>
        <H1 style={{ margin: '20px 0', textTransform: 'uppercase' }}>
          QUÊN MẬT KHẨU
        </H1>
          <form
            onSubmit={handleSubmit}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  //error={isValid || rFailedAuth.status}
                  fullWidth
                  onChange={handleChangeUsername}
                  label="Tên đăng nhập"
                  variant="outlined"
                  id="username"
                  name="username"
                  
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangeEmail}
                  //error={isValid || rFailedAuth.status || inValidEmail}
                  label="Email"
                  variant="outlined"
                  id="email"
                  name="email"
                  type="text"
                />
              </Grid>

              <Grid item xs={12}>
                
                <Button
                  variant="contained"
                  color="primary"
                  // disabled={rIsProcessingAuth}
                  type="submit"
                >
                  GỬI
                </Button>
              </Grid>
              
              
            </Grid>
          </form>
        
      </Container>
    
  );
};
export default
  connect(
    state => ({
      
      //user: state.authReducer.user,
      
    }),
    {
      
    }
  )(ForgotPassword)

