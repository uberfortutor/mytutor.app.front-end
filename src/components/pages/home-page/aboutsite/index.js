import React, { Component } from 'react';
import {
  Grid,
  Container,
  Typography,
  Divider,
  CardMedia,
  Card
} from '@material-ui/core';
import { Button } from '@commons/components';
import { makeStyles } from '@material-ui/core/styles';
import SchoolIcon from '@material-ui/icons/School';
import PeopleIcon from '@material-ui/icons/People';
import { Link } from 'react-router-dom';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
// fontAweSome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHandshake } from '@fortawesome/free-solid-svg-icons';

const useStyles = makeStyles(theme => ({
  containWrapAbout: {
    padding: '3%'
  },
  containAbout: {
    background: 'rgba(179, 179, 179,0.1)',
    boxShadow: '2px 4px 10px 0 rgba(0,0,0,0.1)',
    padding: '30px 0'
  },
  customSizeIcon: {
    maxWidth: '100px',
    height: '100px',
    width: '100% !important'
  },
  customItemCol: {
    color: '#0677BC',
    textAlign: 'center'
  },
  customItemColImage: {
    color: '#0677BC',
    textAlign: 'center',
    paddingLeft: '5%',
    marginTop: '30px'
  },
  customItemColImageContent: {
    color: '#000',
    textAlign: 'left',
    paddingLeft: '20px',
    marginTop: '30px',
    alignSelf: 'center'
  },
  setContrainsHeight: {
    minHeight: '90px',
    maxHeight: '90px'
  },
  customTitle: {
    fontWeight: 'bold',
    fontSize: '2rem'
  },
  customDivider: {
    backgroundColor: '#0677BC',
    height: '2px',
    width: '50%',
    marginLeft: '50%',
    transform: 'translateX(-50%)',
    margin: '30px 0'
  },
  customTitleAbout: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: '2rem'
  },
  cardContain: {
    maxWidth: '100%'
  },
  cardMedia: {
    height: '320px'
  },
  bombTitle: {
    fontSize: '1rem',
    fontWeight: 'bold'
  },
  bombContent: {
    fontStyle: 'italic',
    opacity: '0.5',
    fontSize: '14px'
  },
  customItemContentBomb: {
    paddingLeft: '20px'
  }
}));

const AboutSite = () => {
  const classes = useStyles();
  return (
    <Container className={classes.containWrapAbout}>
      <Grid container className={classes.containAbout}>
        <Grid item xs={4} className={classes.customItemCol}>
          <Link
            to="/list-teacher"
            style={{ color: 'unset', textDecoration: 'unset' }}
          >
            <SchoolIcon className={classes.customSizeIcon} />
            <Typography className={classes.customTitle}>
              1000+ Gia Sư
            </Typography>
            <Typography>Trên toàn quốc</Typography>
          </Link>
        </Grid>
        <Grid item xs={4} className={classes.customItemCol}>
          <PeopleIcon className={classes.customSizeIcon} />
          <Typography className={classes.customTitle}>
            500+ Lượt Truy Cập
          </Typography>
          <Typography>Mỗi ngày</Typography>
        </Grid>
        <Grid item xs={4} className={classes.customItemCol}>
          <FontAwesomeIcon
            icon={faHandshake}
            className={classes.customSizeIcon}
            fixedWidth
          />
          <Typography className={classes.customTitle}>800+ Hợp đồng</Typography>
          <Typography>Thành công</Typography>
        </Grid>
        <Divider className={classes.customDivider} />
        <Grid item xs={12} className={classes.customItemCol}>
          <Typography className={classes.customTitleAbout}>
            HỆ THỐNG TÌM GIA SƯ CHÚNG TÔI
          </Typography>
        </Grid>
        <Grid item xs={5} className={classes.customItemColImage}>
          <Card className={classes.cardContain}>
            <CardMedia
              className={classes.cardMedia}
              image="/media/about-1.jpeg"
            />
          </Card>
        </Grid>
        <Grid item xs={3} className={classes.customItemColImageContent}>
          <Container
            style={{
              display: 'flex',
              padding: '0',
              marginTop: '30px',
              minHeight: '90px',
              maxHeight: '90px'
            }}
          >
            <Grid item xs={2}>
              <ThumbUpIcon color="primary" style={{ fontSize: '3rem' }} />
            </Grid>
            <Grid item xs={10} className={classes.customItemContentBomb}>
              <Typography className={classes.bombTitle}>Chất lượng</Typography>
              <Typography className={classes.bombContent}>
                Gia sư được kiểm duyệt kỹ càng và đánh giá khách quan bởi người
                học
              </Typography>
            </Grid>
          </Container>
          <Container
            style={{
              display: 'flex',
              padding: '0',
              marginTop: '30px',
              minHeight: '90px',
              maxHeight: '90px'
            }}
          >
            <Grid item xs={2}>
              <FontAwesomeIcon
                icon={faHandshake}
                style={{ fontSize: '2.5rem', color: '#0677BC' }}
              />
            </Grid>
            <Grid item xs={10} className={classes.customItemContentBomb}>
              <Typography className={classes.bombTitle}>
                Kết nối trực tiếp
              </Typography>
              <Typography className={classes.bombContent}>
                Kết nối học viên với gia sư trong khu vực theo mô hình Grab,
                Uber
              </Typography>
            </Grid>
          </Container>
        </Grid>
        <Grid item xs={3} className={classes.customItemColImageContent}>
          <Container
            style={{
              display: 'flex',
              padding: '0',
              marginTop: '30px',
              minHeight: '90px',
              maxHeight: '90px'
            }}
          >
            <Grid item xs={2}>
              <AttachMoneyIcon color="primary" style={{ fontSize: '3rem' }} />
            </Grid>
            <Grid item xs={10} className={classes.customItemContentBomb}>
              <Typography className={classes.bombTitle}>
                Tuỳ chọn học phí
              </Typography>
              <Typography className={classes.bombContent}>
                Nhiều gia sư cùng đấu giá mức học phí, bạn chỉ cần lựa chọn học
                phí phù hợp nhất
              </Typography>
            </Grid>
          </Container>
          <Container
            style={{
              display: 'flex',
              padding: '0',
              marginTop: '30px',
              minHeight: '90px',
              maxHeight: '90px'
            }}
          >
            <Grid item xs={2}>
              <SchoolIcon color="primary" style={{ fontSize: '3rem' }} />
            </Grid>
            <Grid item xs={10} className={classes.customItemContentBomb}>
              <Typography className={classes.bombTitle}>
                Lựa chọn đa dạng
              </Typography>
              <Typography className={classes.bombContent}>
                Gia sư là giảng viên, sinh viên, ... với nhiều ngành nghề khác
                nhau
              </Typography>
            </Grid>
          </Container>
        </Grid>
        <Grid
          item
          xs={12}
          className={classes.customItemCol}
          style={{ marginTop: '30px' }}
        >
          <Link
            to="/sign-up-learner"
            style={{ color: 'white', textDecoration: 'unset' }}
          >
            <Button
              variant="contained"
              color="primary"
              style={{ width: 'unset' }}
            >
              ĐĂNG KÍ TÌM GIA SƯ NGAY
            </Button>
          </Link>
        </Grid>
      </Grid>
    </Container>
  );
};

export default AboutSite;
