import React, { Component } from 'react';
import { get } from 'lodash';
import { Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TutorComponent from '@components/tutor-component';
import { restClient } from '@supports/rest-client';

const useStyles = makeStyles({
  root: {
    padding: '30px 0 90px 00'
  },
  titleTop: {
    padding: 20,
    fontWeight: 'bold',
    marginBottom: 30
  }
});
const TopTutor = () => {
  const classes = useStyles();
  const [highRateTutors, setHighRateTutors] = React.useState([]);
  const [loadedHighRateTutors, setLoadedHighRateTutors] = React.useState(false);

  React.useEffect(() => {
    if (!loadedHighRateTutors) {
      setLoadedHighRateTutors(true);

      restClient.asyncGet('/tutors/high-rate').then(res => {
        console.log('res', res);
        setHighRateTutors(get(res, ['data', 'tutors'], []) || []);
      });
    }
  });
  console.log('high rate tutors', highRateTutors);

  return (
    <div className={classes.root}>
      <Typography
        align="center"
        variant="h4"
        className={classes.titleTop}
        color="primary"
      >
        GIA SƯ NỔI BẬT
      </Typography>
      <Grid container spacing={2}>
        {highRateTutors.map((tutor, index) => (
          <Grid item xs={4} key={index}>
            <TutorComponent tutorInfo={tutor} top={index + 1} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default TopTutor;
