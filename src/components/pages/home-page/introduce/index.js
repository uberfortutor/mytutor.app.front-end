import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@commons/components';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { CardMedia } from '@material-ui/core';
import './custom.css';

const baseUrl = '/media';

function Introduce() {
  const settings = {
    dots: true,
    lazyLoad: true,
    autoplay: true,
    autoplaySpeed: 3000,
    infinite: true,
    fade: true,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true
  };
  return (
    <div style={{ position: 'relative' }}>
      <Slider {...settings}>
        <div>
          <img src={baseUrl + '/intro-2.jpg'} className="imgShow" alt="hinh" />
          <div className="intro__content">
            <span className="intro__content__title">Tìm gia sư 4.0</span>
            <div className="intro__content__description">
              <div>
                HÃY ĐỂ CHÚNG TÔI TÌM GIA SƯ <br /> CHẤT LƯỢNG VÀ PHÙ HỢP NHẤT
              </div>
              <div style={{ marginTop: '10px' }}>
                CHỈ VỚI <strong style={{ color: '#0677BC' }}>5 PHÚT</strong>{' '}
                HOÀN TẤT VIỆC ĐĂNG KÍ
              </div>
            </div>
            
            <Link to="sign-up-learner" className="intro__content__button">
              <Button
                variant="contained"
                color="primary"
                style={{ width: 'auto', marginTop: '20px' }}
              >
                Đăng kí ngay
              </Button>
            </Link>
          </div>
        </div>

        <div>
          <img src={baseUrl + '/intro-3.jpg'} className="imgShow" alt="hinh" />
          <div className="intro__content">
            <span className="intro__content__title">Trở thành gia sư 4.0</span>
            <div className="intro__content__description">
              <div>
                HÃY THAM GIA CÙNG CHÚNG TÔI
                <br /> XÂY DỰNG HỆ THỐNG GIA SƯ CHẤT LƯỢNG{' '}
              </div>
              <div style={{ marginTop: '10px' }}>
                VỚI HƠN <strong style={{ color: '#0677BC' }}>1000+</strong> GIA
                SƯ TRÊN TOÀN QUỐC
              </div>
            </div>
            
            <Link to="sign-up-tutor" className="intro__content__button">
              <Button
                variant="contained"
                color="primary"
                style={{ width: 'auto', marginTop: '20px' }}
              >
                THAM GIA NGAY
              </Button>
            </Link>
          </div>
        </div>
      </Slider>
    </div>
  );
}
export default Introduce;
