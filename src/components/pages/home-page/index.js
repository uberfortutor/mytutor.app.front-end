import React from 'react';
// import { withRouter } from 'react-router';
import '@globals/index.css';
import Fade from '@material-ui/core/Fade';
// import Headroom from 'react-headroom';
import { Redirect } from 'react-router-dom';
// import axios from 'axios';
import { makeStyles } from '@material-ui/core';
import { getCookie } from '@supports/utils/cookies';
import Introduce from './introduce';
import AboutSite from './aboutsite';
import TopTutor from './top-tutor';

const useStyles = makeStyles(theme => ({}));

function HomePage() {
  const classes = useStyles();
  // console.log('index', getCookie('isNew'));
  return (
    <div className={classes.content}>
      {getCookie('isNew') === 'true' ? (
        <Redirect to="/manager-info/profile-update" />
      ) : (
        ''
      )}
      <Fade>
        <Introduce />
      </Fade>
      <Fade>
        <AboutSite />
      </Fade>
      <Fade>
        <TopTutor />
      </Fade>
    </div>
  );
}

export default HomePage;
