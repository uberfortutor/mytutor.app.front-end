import React from 'react';
import { Container, Grid, TextField } from '@material-ui/core';
import { withRouter } from 'react-router';
import queryString from 'query-string';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { get, isEmpty } from 'lodash';
import { H1, Button } from '@commons/components';
import WarningIcon from '@material-ui/icons/Warning';
import { signOut } from '@supports/store/redux/actions';
import FlatErrorMessage from '../../../commons/components/flat-error-message';
import { restClient } from '../../../supports/rest-client';

const useStyles = makeStyles({
  containerBody: {
    position: 'relative',
    margin: '60px auto',
    padding: 20,
    background: '#fff',
    boxShadow: '2px 4px 20px 0 rgba(0,0,0,0.2)'
  },
  errNoti: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'red',
    textAlign: 'center',
    marginBottom: '20px'
  },

  showLoading: {
    padding: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',

    display: 'none',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingComponent: {
    textAlign: 'center',
    color: '#0677BC'
  }
});

const ResetPassword = ({ location, token, actions }) => {
  const classes = useStyles();
  const [newPassword, setNewPassword] = React.useState('');
  const [newPasswordError, setNewPasswordError] = React.useState(null);
  const [isProcessing, setIsProcessing] = React.useState(false);
  const [resetStatus, setResetStatus] = React.useState(false);
  const [messageFail, setMessageFail] = React.useState('');
  const resetToken = get(queryString.parse(location.search), 'token', null);

  const resetPassword = () => {
    setIsProcessing(true);

    restClient
      .asyncPost('/auth/reset-password', { token: resetToken, newPassword })
      .then(res => {
        if (get(res, ['data', 'status'])) {
          setResetStatus(true);
        } else {
          setMessageFail(get(res, ['data', 'message']));
        }

        setIsProcessing(false);
      })
      .catch(err => {
        console.log('error: ', err);
        if (get(err, ['response', 'status']) === 403) {
          setMessageFail(
            'Tài khoản được được đăng ký với email này đã bị khóa.'
          );
          return;
        }
        setMessageFail(get(err, ['response', 'data']));
      });
  };

  const handleChangeNewPassword = e => {
    setNewPassword(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();

    setMessageFail('');

    if (isEmpty(newPassword)) {
      setNewPasswordError('Vui lòng nhập mật khẩu mới của bạn.');
      return;
    } else {
      setNewPasswordError(null);
    }

    resetPassword();
  };

  if (token) {
    actions.signOut();
  }

  return (
    <Container maxWidth="sm" className={classes.containerBody}>
      <H1 style={{ margin: '20px 0', textTransform: 'uppercase' }}>
        Nhập mật khẩu mới
      </H1>

      {resetStatus ? (
        <div>
          Mật khẩu của bạn đã được đổi mới thành công. Giờ bạn có thể đăng nhập
          vào hệ thống.
          <br />
          <Link to="/sign-in" style={{ textDecoration: 'none' }}>
            <Button variant="contained" color="primary">
              Đăng nhập
            </Button>
          </Link>
        </div>
      ) : (
        <form onSubmit={handleSubmit}>
          <FlatErrorMessage message={messageFail} />
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                onChange={handleChangeNewPassword}
                error={newPasswordError || false}
                label="Mật khẩu mới"
                variant="outlined"
                id="newPassword"
                name="newPassword"
                type="password"
                value={newPassword}
              />
            </Grid>

            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                disabled={isProcessing}
                type="submit"
              >
                GỬI
              </Button>
            </Grid>
          </Grid>
        </form>
      )}
    </Container>
  );
};
export default withRouter(
  connect(
    state => ({ token: get(state, ['authReducer', 'user', 'token']) }),
    dispatch => ({
      actions: bindActionCreators({ signOut }, dispatch)
    })
  )(ResetPassword)
);
