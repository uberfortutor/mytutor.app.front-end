import React from 'react';
import {
  Container,
  Typography,
  Grid,
  TextField,
  Box,
  CircularProgress,
  Fade
} from '@material-ui/core';
import { withRouter } from 'react-router';
import Select from 'react-select';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { get, isEmpty } from 'lodash';
import UserLayout from '@containers/layouts/user-layout';
import {
  H1,
  FacebookIcon,
  GoogleIcon,
  Link,
  Button
} from '@commons/components';
import {
  signIn,
  sendingEmailVerifyAgain,
  authViaSocial,
  updateProfileAfterLoginSocial
} from '@supports/store/redux/actions';
import WarningIcon from '@material-ui/icons/Warning';

const useStyles = makeStyles({
  containerBody: {
    position: 'relative',
    margin: '60px auto',
    padding: 20,
    background: '#fff',
    boxShadow: '2px 4px 20px 0 rgba(0,0,0,0.2)'
  },
  errNoti: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'red',
    textAlign: 'center',
    marginBottom: '20px'
  },
  sendEmailAgain: {
    cursor: 'pointer',
    fontStyle: 'italic',
    fontWeight: 'bold',
    '&:hover': {
      textDecoration: 'underline'
    }
  },
  showLoading: {
    padding: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',

    display: 'none',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingComponent: {
    textAlign: 'center',
    color: '#0677BC'
  },
  emailSent: {
    textAlign: 'center',
    color: '#0677BC',
    padding: '20px',
    background: '#f9f9f9',
    boxShadow: '2px 4px 9px 0 rgba(0,0,0,0.2)',
    transform: 'translateX(-20px)'
  }
});
const roles = [
  { value: 'tutor', label: 'Người dạy' },
  { value: 'finder', label: 'Người học' }
];

// rFailedAuth : login fail -> change to true
const LogIn = ({
  updateProfileSocial,
  rViaSocial,
  rFailedAuth,
  user,
  rIsProcessingAuth,
  SignIn,
  rIsSendingEmail,
  rSentEmail,
  VerifyEmail,
  authSocial,
  history
}) => {
  const classes = useStyles();
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const [usernameU, setUsernameU] = React.useState('');
  const [passwordU, setPasswordU] = React.useState('');
  const [emailU, setEmailU] = React.useState('');
  const [intiEmailU, setInitEmailU] = React.useState(false);
  const [isInvalidUsernameU, setInvalidUsernameU] = React.useState(false);
  const [isInvalidPasswordU, setInvalidPasswordU] = React.useState(false);
  const [isInvalidEmailU, setInvalidEmailU] = React.useState(false);
  const [social, setSocial] = React.useState('');

  const [isValid, setIsValid] = React.useState(false);
  const [role, setRole] = React.useState(roles[1]);
  const [messageFail, setMessageFail] = React.useState('');

  const handleChangeUsername = e => {
    isValid ? setIsValid(!isValid) : '';
    setMessageFail('');
    rFailedAuth.status = false;
    setUsername(e.target.value);
  };
  const handleChangeUsernameU = e => {
    isValid ? setIsValid(!isValid) : '';
    setMessageFail('');
    rFailedAuth.status = false;
    setUsernameU(e.target.value);
  };
  const handleChangeRole = role => {
    setRole(role);
  };
  const handleChangePassword = e => {
    isValid ? setIsValid(!isValid) : '';
    setMessageFail('');
    rFailedAuth.status = false;
    setPassword(e.target.value);
  };
  const handleChangePasswordU = e => {
    isValid ? setIsValid(!isValid) : '';
    setMessageFail('');
    rFailedAuth.status = false;
    setPasswordU(e.target.value);
  };
  const handleChangeEmailU = e => {
    isValid ? setIsValid(!isValid) : '';
    setMessageFail('');
    rFailedAuth.status = false;
    setEmailU(e.target.value);
  };
  const handleSendEmail = () => {
    // call api send Email
    VerifyEmail({ username, password });
    // redirect login Again
  };

  const loginViaGoogle = () => {
    authSocial('google');
    setSocial('google');
  };
  const loginViaFacebook = () => {
    authSocial('facebook');
    setSocial('facebook');
  };
  const handleSubmitU = e => {
    e.preventDefault();

    let isValidSubmit = true;
    if (isEmpty(usernameU)) {
      setInvalidUsernameU(true);
      isValidSubmit = false;
    } else {
      setInvalidUsernameU(false);
    }

    if (isEmpty(passwordU)) {
      setInvalidPasswordU(true);
      isValidSubmit = false;
    } else {
      setInvalidPasswordU(false);
    }

    if (isEmpty(emailU)) {
      setInvalidEmailU(true);
      isValidSubmit = false;
    } else {
      setInvalidEmailU(false);
    }

    if (!isValidSubmit) {
      setIsValid(!isValid);
      setMessageFail('Một số thông tin còn bỏ trống. Vui lòng kiểm tra lại!');
      return;
    } else {
      const iRole = get(role, 'value');
      const newUser = {
        ...rViaSocial.info,
        username: usernameU,
        password: passwordU,
        email: emailU,
        role: iRole
      };
      updateProfileSocial(social, newUser);
    }
  };

  const handleSubmit = e => {
    e.preventDefault();

    if (isEmpty(username) || isEmpty(password)) {
      setIsValid(!isValid);
      setMessageFail(
        'Tài khoản hoặc mật khẩu không được bỏ trống. Mời kiểm tra lại !'
      );
      return;
    } else {
      SignIn({ username, password });
    }
  };

  if (rViaSocial.isExists === false) {
    if (!intiEmailU) {
      setInitEmailU(true);
      setEmailU(get(rViaSocial, ['info', 'email'], ''));
    }
  }

  if (user.token) {
    const destUrl = get(history, ['location', 'state', 'destUrl']);
    return <Redirect to={destUrl || '/'} />;
  }

  return (
    <UserLayout pageTitle="Đăng nhập">
      <Container maxWidth="sm" className={classes.containerBody}>
        <H1 style={{ margin: '20px 0', textTransform: 'uppercase' }}>
          {rViaSocial.isExists === false && rViaSocial.info
            ? 'Đăng ký thông tin đăng nhập'
            : 'Đăng nhập'}
        </H1>

        {isValid || rFailedAuth.status ? (
          <div className={classes.errNoti}>
            <WarningIcon />
            <Box>
              {messageFail || rFailedAuth.message}
              {rFailedAuth.verifyAgain ? (
                <Box
                  className={classes.sendEmailAgain}
                  onClick={handleSendEmail}
                >
                  Bạn có muốn gửi lại email xác nhận?
                </Box>
              ) : (
                ''
              )}
            </Box>
          </div>
        ) : (
          ''
        )}
        <Container
          className={classes.showLoading}
          style={
            rSentEmail.status || rIsSendingEmail
              ? { zIndex: 1, display: 'flex' }
              : {}
          }
        >
          <Fade
            style={rIsSendingEmail ? { display: 'block' } : { display: 'none' }}
            in={rIsSendingEmail}
          >
            <Box className={classes.loadingComponent}>
              <CircularProgress />
            </Box>
          </Fade>
          <Fade
            style={rIsSendingEmail ? { display: 'none' } : { display: 'block' }}
            in={rSentEmail.status}
          >
            <Box className={classes.emailSent}>
              Kiểm tra email đã đăng kí với{' '}
              <strong style={{ fontStyle: 'italic' }}>{username}</strong> để xác
              thực tài khoản!
              <Link href="/sign-in">
                <Button
                  variant="contained"
                  color="primary"
                  style={{ maxWidth: '100px', marginTop: '20px' }}
                >
                  Đóng
                </Button>
              </Link>
            </Box>
          </Fade>
        </Container>
        <Container
          className={classes.showLoading}
          style={rIsProcessingAuth ? { zIndex: 1, display: 'flex' } : {}}
        >
          <Fade in={rIsProcessingAuth}>
            <Box className={classes.loadingComponent}>
              <Typography style={{ fontWeight: '600', fontSize: '18px' }}>
                {rIsProcessingAuth ? 'Đang đăng nhập' : ''}
              </Typography>
              <CircularProgress />
            </Box>
          </Fade>
        </Container>
        {rViaSocial.isExists === false && rViaSocial.info ? (
          <form
            onSubmit={handleSubmitU}
            style={rIsProcessingAuth ? { opacity: '0.2' } : {}}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  error={isInvalidUsernameU}
                  fullWidth
                  onChange={handleChangeUsernameU}
                  label="Tên đăng nhập"
                  variant="outlined"
                  id="usernameU"
                  name="usernameU"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangePasswordU}
                  error={isInvalidPasswordU}
                  label="Mật khẩu"
                  variant="outlined"
                  id="passwordU"
                  name="passwordU"
                  type="password"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangeEmailU}
                  error={isInvalidEmailU}
                  defaultValue={get(rViaSocial, ['info', 'email'], '')}
                  label="Email"
                  variant="outlined"
                  id="emailU"
                  name="emailU"
                  type="email"
                />
              </Grid>
              <Grid item xs={12}>
                <Typography color="textPrimary" gutterBottom>
                  Vai trò
                </Typography>

                <Select
                  value={role}
                  onChange={handleChangeRole}
                  options={roles}
                  placeholder="Chọn"
                />
              </Grid>
              <Grid item xs={12}>
                {user.token && !rIsProcessingAuth ? <Redirect to="/" /> : ''}
                <Button
                  variant="contained"
                  color="primary"
                  // disabled={rIsProcessingAuth}
                  type="submit"
                >
                  Đăng ký tài khoản với thông tin {rViaSocial.type}
                </Button>
              </Grid>
            </Grid>
          </form>
        ) : (
          <form
            onSubmit={handleSubmit}
            style={
              rIsProcessingAuth || rIsSendingEmail || rSentEmail.status
                ? { opacity: '0.2' }
                : {}
            }
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  error={isValid || rFailedAuth.status}
                  fullWidth
                  onChange={handleChangeUsername}
                  label="Tên đăng nhập"
                  variant="outlined"
                  id="username"
                  name="username"
                  helperText={
                    isValid
                      ? 'Tài khoản không được bỏ trống. Mời nhập thông tin'
                      : rFailedAuth.status &&
                        (rFailedAuth.locked || rFailedAuth.verifyAgain)
                      ? ''
                      : rFailedAuth.status
                      ? 'Hãy kiểm tra lại Tên đăng nhập của bạn!'
                      : ''
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangePassword}
                  error={isValid || rFailedAuth.status}
                  label="Mật khẩu"
                  variant="outlined"
                  id="password"
                  name="password"
                  type="password"
                  helperText={
                    isValid
                      ? 'Mật khẩu không được bỏ trống. Mời nhập thông tin'
                      : rFailedAuth.status &&
                        (rFailedAuth.locked || rFailedAuth.verifyAgain)
                      ? ''
                      : rFailedAuth.status
                      ? 'Hãy kiểm tra lại Mật khẩu của bạn!'
                      : ''
                  }
                />
              </Grid>

              <Grid item xs={12}>
                {user.token && !rIsProcessingAuth ? (
                  <>
                    <Redirect to="/" />
                  </>
                ) : (
                  ''
                )}
                <Button
                  variant="contained"
                  color="primary"
                  // disabled={rIsProcessingAuth}
                  type="submit"
                >
                  Đăng nhập
                </Button>
              </Grid>
              <Grid item xs={12}>
                <Typography
                  variant="body1"
                  gutterBottom
                  color="textSecondary"
                  align="center"
                >
                  hoặc
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="outlined"
                  border="1px solid #FF8585"
                  googlebtn="google"
                  loginViaType="social"
                  onClick={loginViaGoogle}
                >
                  <GoogleIcon /> Đăng nhập bằng tài khoản google
                </Button>
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="outlined"
                  color="primary"
                  loginViaType="social"
                  onClick={loginViaFacebook}
                >
                  <FacebookIcon /> Đăng nhập bằng tài khoản facebook
                </Button>
              </Grid>
              <Grid item xs={12}>
                <Link href="/forgot-password" style={{ textAlign: 'center' }}>
                  Quên mật khẩu ?
                </Link>
              </Grid>
              <Grid
                item
                xs={12}
                justify="center"
                alignItems="center"
                style={{ display: 'flex' }}
              >
                <span style={{ fontStyle: 'italic', marginRight: 10 }}>
                  Bạn chưa có tài khoản?
                </span>
                <Link href="/sign-up" style={{ textAlign: 'center' }}>
                  Đăng kí ngay
                </Link>
              </Grid>
            </Grid>
          </form>
        )}
      </Container>
    </UserLayout>
  );
};
export default withRouter(
  connect(
    state => ({
      rFailedAuth: state.authReducer.failedAuth,
      rIsProcessingAuth: state.authReducer.isProcessingAuth,
      rIsSendingEmail: state.authReducer.isSendingEmail,
      rSentEmail: state.authReducer.sentEmail,
      user: state.authReducer.user,
      rViaSocial: state.authReducer.viaSocial
    }),
    {
      SignIn: signIn,
      VerifyEmail: sendingEmailVerifyAgain,
      authSocial: authViaSocial,
      updateProfileSocial: updateProfileAfterLoginSocial
    }
  )(LogIn)
);
