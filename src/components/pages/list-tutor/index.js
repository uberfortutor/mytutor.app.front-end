import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';
import { get, isEmpty, filter } from 'lodash';
// import InfiniteScroll from 'react-infinite-scroller';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  Typography,
  Grid,
  Box,
  Collapse,
  Container,
  Slider,
  RadioGroup,
  Chip,
  TextField,
  FormControlLabel,
  Radio,
  Button,
  InputLabel,
  Select,
  Tooltip
} from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import Autocomplete from '@material-ui/lab/Autocomplete';
import * as mainActions from '@supports/store/redux/actions/main.actions';
import { bindActionCreators } from 'redux';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom';
import { getCookie } from '@supports/utils/cookies';
import TutorComponent from '@components/tutor-component';
import Loading from '@commons/components/loading';
import { Fade } from 'react-reveal';
import dataAddress from '@globals/dataAddress';
import './index.css';
import { restClient } from '../../../supports/rest-client';
import Empty from '../../../commons/components/empty';

const useStyles = makeStyles({
  root: {
    padding: '30px 0 60px 00'
  },
  titleTop: {
    padding: 20,
    fontWeight: 'bold',
    marginBottom: 30
  },
  forPagination: {
    marginTop: 30,
    textAlign: 'center'
  },
  headeListTutor: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
    position: 'relative',
    zIndex: '10'
  },
  featureHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    alignSeft: 'center'
  },
  filterDropdown: {
    marginBottom: 10,
    position: 'relative',
    zIndex: '10'
  },
  titleListTutor: {
    fontSize: '30px',
    textTransform: 'uppercase',
    color: '#0677BC',
    fontWeight: 'bold'
  },
  titleFilter: {
    fontWeight: 'bold',
    textTransform: 'uppercase',
    marginBottom: '30px',
    color: '#0677BC'
  }
});

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  return (
    <Tooltip placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

ValueLabelComponent.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired
};

const ListTutor = ({ actions, skills, isGettingSkill }) => {
  const classes = useStyles();
  const pageSize = 12;

  const [needToFilter, setNeedToFilter] = React.useState(false);
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);

  const [isCollapse, setIsCollapse] = React.useState(false);
  const [hasMore, setHasMore] = React.useState(true);
  const [page, setPage] = React.useState(1);
  const [listTutor, setListTutor] = React.useState([]);
  const [skillOptions, setSkillOptions] = React.useState(skills || []);

  const [selectSkill, setSelectSkill] = React.useState([]);
  const [province, setProvince] = React.useState('');
  const [ward, setWard] = React.useState('');
  const [isIncrease, setIsIncrease] = React.useState('false');
  const [value, setValue] = React.useState([0, 1000000]);
  const districts = get(dataAddress[province], 'districts');

  const handleChangeIncrease = e => {
    setIsIncrease(e.target.value);
  };

  const loadMoreTutors = () => {
    // if (!hasMore) {
    //   return;
    // }

    setHasMore(false);

    restClient
      .asyncGet('/tutors', {
        page,
        pageSize,
        increaseTuition: isIncrease,
        province: isEmpty(province) ? undefined : province,
        ward: isEmpty(ward) ? undefined : ward,
        skills: selectSkill.map(skl => skl.id).join(',')
      })
      .then(res => {
        const { tutors } = res.data;
        setIsLoading(false);

        if (tutors) {
          setListTutor([...listTutor, ...tutors]);
          setPage(page + 1);
          if (tutors.length === 0) {
            setHasMore(false);
            return;
          }

          setHasMore(true);
        }
      })
      .catch(err => {
        setHasMore(false);
        console.log(err.response);
      });
  };

  React.useEffect(() => {
    if (isEmpty(skillOptions)) {
      setSkillOptions(skills || []);
    }

    if (!isLoaded) {
      setIsLoaded(true);
      setIsLoading(true);
      setNeedToFilter(true);
    }
  });

  React.useEffect(() => {
    if (needToFilter) {
      console.log('filtering');
      setNeedToFilter(false);
      loadMoreTutors();
    }
  }, [needToFilter]);

  const handleFilter = () => {
    setIsCollapse(false);
    setPage(1);
    setListTutor([]);
    setIsLoading(true);
    setNeedToFilter(true);
    // const stringSkill = selectSkill
    //   .map(skillOptions => {
    //     return skillOptions.id;
    //   })
    //   .join(',');
    // const data = {
    //   skills: stringSkill,
    //   province: province,
    //   ward: ward,
    //   increaseTuition: isIncrease,
    //   minTuition: value[0],
    //   maxTuition: value[1]
    // };
    // restClient
    //   .asyncGet('/tutors', data)
    //   .then(res => {
    //     if (res.data) {
    //       console.log(res.data);
    //       setListTutor(get(res.data, 'tutors'));
    //     }
    //   })
    //   .catch(err => console.log(err.response));
  };

  const handleChangeSkill = (event, val) => {
    const test = [];
    val.forEach(i => {
      const a = filter(skills, r => {
        return r.title === i;
      });
      test.push(a[0]);
    });
    setSelectSkill(test);
  };

  const toggleFilter = () => {
    setIsCollapse(!isCollapse);
    if (isEmpty(skillOptions)) actions.getSkills();
    console.log(skillOptions);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeSelectCity = e => {
    setProvince(e.target.value);
  };

  const handleChangeSelectDistrict = e => {
    setWard(e.target.value);
  };

  return (
    <div className={classes.root}>
      {getCookie('isNew') === 'true' ? (
        <Redirect to="/manager-info/profile-update" />
      ) : (
        ''
      )}
      <Container className={classes.headeListTutor}>
        <Container>
          <Typography component="span" className={classes.titleListTutor}>
            Danh sách gia sư
          </Typography>
        </Container>
        <Container className={classes.featureHeader}>
          <Box style={{ alignItems: 'center', cursor: 'pointer' }}>
            <Box onClick={toggleFilter}>
              <FilterListIcon color="primary" />
            </Box>
          </Box>
        </Container>
      </Container>
      <Collapse in={isCollapse} className={classes.filterDropdown}>
        <Grid container spacingOne>
          <Grid container item xs={12} md={4} style={{ padding: '0 20px' }}>
            <div className={classes.titleFilter}>Tìm theo giá dạy</div>

            <Slider
              value={value}
              onChange={handleChange}
              valueLabelDisplay="on"
              ValueLabelComponent={ValueLabelComponent}
              // aria-labelledby="range-slider"
              max={1000000}
              step={10000}
            />
            <RadioGroup
              aria-label="tuition"
              name="tuition"
              value={isIncrease}
              onChange={handleChangeIncrease}
            >
              <FormControlLabel
                value="true"
                control={<Radio />}
                label="Giá tăng dần"
                name="true"
              />
              <FormControlLabel
                name="false"
                value="false"
                control={<Radio />}
                label="Giá giảm dần"
              />
            </RadioGroup>
          </Grid>
          <Grid container xs={12} md={4} style={{ padding: '0 20px' }}>
            <div className={classes.titleFilter}>Tìm theo địa chỉ</div>
            <Grid item xs={12}>
              <InputLabel htmlFor="province">Tỉnh/Thành phố</InputLabel>
              <Select
                native
                fullWidth
                value={province}
                onChange={handleChangeSelectCity}
                inputProps={{
                  name: 'province',
                  id: 'province'
                }}
              >
                <option selected> </option>
                {Object.keys(dataAddress).map(i => {
                  return <option value={i}>{dataAddress[i].name}</option>;
                })}
              </Select>
            </Grid>
            <Grid item xs={12}>
              <InputLabel htmlFor="ward">Quận/Huyện</InputLabel>
              <Select
                native
                fullWidth
                value={ward}
                onChange={handleChangeSelectDistrict}
                inputProps={{
                  name: 'ward',
                  id: 'ward'
                }}
              >
                <option selected disabled>
                  ---
                </option>
                {districts
                  ? Object.keys(districts).map(function(key) {
                      return <option value={key}>{districts[key]}</option>;
                    })
                  : ''}
              </Select>
            </Grid>
          </Grid>
          <Grid container xs={12} md={4} style={{ padding: '0 20px' }}>
            <div className={classes.titleFilter}>Tìm theo kỹ năng</div>

            <Grid item xs={12}>
              <Autocomplete
                multiple
                fullWidth
                // error={isEmpty(skillOptions)}
                style={{ width: '100%' }}
                id="skills"
                options={
                  !isEmpty(skillOptions)
                    ? skillOptions.map(option => option.title)
                    : []
                }
                onChange={handleChangeSkill}
                filterSelectedOptions
                renderTags={(value, getTagProps) =>
                  value.map((option, index) => (
                    <Chip
                      variant="outlined"
                      label={option}
                      {...getTagProps({ index })}
                    />
                  ))
                }
                renderInput={params => (
                  <TextField
                    {...params}
                    variant="standard"
                    label="Danh sách kỹ năng"
                    multiline="true"
                    rowsMax={1}
                    placeholder="Nhập kĩ năng"
                    fullWidth
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} />
          </Grid>
          <Button
            onClick={handleFilter}
            variant="outlined"
            color="primary"
            style={{ margin: 'auto', marginTop: '20px' }}
          >
            Lọc
          </Button>
        </Grid>
      </Collapse>
      <Container>
        <InfiniteScroll
          dataLength={listTutor.length} //This is important field to render the next data
          next={() => {
            if (!hasMore) {
              return;
            }
            console.log('next');
            setHasMore(false);
            setNeedToFilter(true);
            // loadMoreTutors();
          }}
          hasMore={isLoading ? false : hasMore}
          loader={<Loading size={40} />}
          scrollableTarget="layoutContainer"
          className="list-tutor-infinite-loop"
        >
          <Grid container spacing={2} id="list-tutor">
            {Object.values(listTutor).map(tutor => {
              return (
                <Grid item xs={12} md={3} key={tutor.id}>
                  <Fade>
                    <TutorComponent tutorInfo={tutor} />
                  </Fade>
                </Grid>
              );
            })}
          </Grid>
        </InfiniteScroll>
      </Container>
      {isLoading ? <Loading size={40} /> : null}
      {!isLoading && !hasMore && isEmpty(listTutor) ? <Empty /> : null}
    </div>
  );
};
const mapStateToProps = state => ({
  skills: get(state, ['mainReducer', 'skills'])
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getSkills: mainActions.getSkills
    },
    dispatch
  )
});
export default connect(mapStateToProps, mapDispatchToProps)(ListTutor);
