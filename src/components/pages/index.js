import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Loadable from 'react-loadable';
import { isNotNewUser, withPageTitle } from '@supports/hoc';
import Loading from '@components/loading';

const SignInPage = Loadable({
  loader: () => import('./sign-in'),
  loading: Loading
});

const SignUpPage = Loadable({
  loader: () => import('./sign-up'),
  loading: Loading
});

const SignUpLearnerPage = Loadable({
  loader: () => import('./sign-up/for-learner'),
  loading: Loading
});

const SignUpTutorPage = Loadable({
  loader: () => import('./sign-up/for-tutor'),
  loading: Loading
});

const UserPage = Loadable({
  loader: () => import('./user-page'),
  loading: 'loading'
});

export default function Page() {
  console.log('page');
  return (
    <Router>
      <Switch>
        <Route
          path="/sign-in"
          exact
          component={withPageTitle(SignInPage, 'Đăng nhập')}
        />
        <Route
          path="/sign-up"
          exact
          component={withPageTitle(SignUpPage, 'Đăng ký')}
        />
        <Route
          path="/sign-up-tutor"
          exact
          component={withPageTitle(SignUpTutorPage, 'Đăng ký tài khoản gia sư')}
        />
        <Route
          path="/sign-up-learner"
          exact
          component={withPageTitle(
            SignUpLearnerPage,
            'Đăng ký tài khoản người học'
          )}
        />
        <Route path="/" component={isNotNewUser(UserPage)} />
        <Route path="*">404 - Not Found!</Route>
      </Switch>
    </Router>
  );
}
