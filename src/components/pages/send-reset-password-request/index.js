import React from 'react';
import {
  Container,
  Typography,
  Grid,
  TextField,
  Box,
  CircularProgress,
  Fade
} from '@material-ui/core';
import { withRouter } from 'react-router';
import Select from 'react-select';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { get, isEmpty } from 'lodash';
import { H1, Link, Button } from '@commons/components';
import { bindActionCreators } from 'redux';
import WarningIcon from '@material-ui/icons/Warning';
import { signOut } from '@supports/store/redux/actions';
import FlatErrorMessage from '../../../commons/components/flat-error-message';
import { restClient } from '../../../supports/rest-client';

const useStyles = makeStyles({
  containerBody: {
    position: 'relative',
    margin: '60px auto',
    padding: 20,
    background: '#fff',
    boxShadow: '2px 4px 20px 0 rgba(0,0,0,0.2)'
  },
  errNoti: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'red',
    textAlign: 'center',
    marginBottom: '20px'
  },

  showLoading: {
    padding: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',

    display: 'none',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingComponent: {
    textAlign: 'center',
    color: '#0677BC'
  }
});

const SendResetPasswordRequest = ({ token, actions }) => {
  const classes = useStyles();
  const [email, setEmail] = React.useState('');
  const [emailError, setEmailError] = React.useState(null);
  const [isProcessing, setIsProcessing] = React.useState(false);
  const [requestStatus, setRequestStatus] = React.useState(false);
  const [messageFail, setMessageFail] = React.useState('');

  const sendResetPasswordRequest = () => {
    setIsProcessing(true);

    restClient
      .asyncPost('/auth/reset-password-through-email', { email })
      .then(res => {
        if (get(res, ['data', 'status'])) {
          //notify success
          // setEmail('');
          setRequestStatus(true);
        } else {
          setMessageFail(get(res, ['data', 'message']));
        }

        setIsProcessing(false);
      })
      .catch(err => {
        console.log('error: ', err);
        if (get(err, ['response', 'status']) === 403) {
          setMessageFail(
            'Tài khoản được được đăng ký với email này đã bị khóa.'
          );
          return;
        }
        setMessageFail(get(err, ['response', 'data']));
      });
  };

  const handleChangeEmail = e => {
    setEmail(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();

    setMessageFail('');

    if (isEmpty(email)) {
      setEmailError('Vui lòng nhập email của bạn.');
      return;
    } else {
      setEmailError(null);
    }

    sendResetPasswordRequest();
  };

  if (token) {
    actions.signOut();
  }

  return (
    <Container maxWidth="sm" className={classes.containerBody}>
      <H1 style={{ margin: '20px 0', textTransform: 'uppercase' }}>
        GỬI YÊU CẦU KHÔI PHỤC MẬT KHẨU
      </H1>
      {requestStatus ? (
        <div>
          Yêu cầu khôi phục mật khẩu của bạn đã được gửi vào hộp thư của bạn.{' '}
          <br />
          hãy kiểm tra hộp thư để có thể nhanh chóng khôi phục lại mật khẩu cho
          mình nhé.
        </div>
      ) : (
        <form onSubmit={handleSubmit}>
          <FlatErrorMessage message={messageFail} />
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                onChange={handleChangeEmail}
                error={emailError || false}
                label="Email"
                variant="outlined"
                id="email"
                name="email"
                type="email"
                value={email}
              />
            </Grid>

            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                disabled={isProcessing}
                type="submit"
              >
                GỬI
              </Button>
            </Grid>
          </Grid>
        </form>
      )}
    </Container>
  );
};

export default connect(
  state => ({ token: get(state, ['authReducer', 'user', 'token']) }),
  dispatch => ({
    actions: bindActionCreators({ signOut }, dispatch)
  })
)(SendResetPasswordRequest);
