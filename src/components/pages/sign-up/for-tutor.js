import React from 'react';
import {
  Container,
  Typography,
  Grid,
  TextField,
  Box,
  CircularProgress,
  Fade
} from '@material-ui/core';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import UserLayout from '@containers/layouts/user-layout';
import {
  H1,
  FacebookIcon,
  GoogleIcon,
  Input,
  Link,
  Button
} from '@commons/components';
import {
  signUp,
  sendingEmailVerifyAgain,
  authViaSocial,
  updateProfileAfterLoginSocial
} from '@supports/store/redux/actions';
import WarningIcon from '@material-ui/icons/Warning';

const useStyles = makeStyles({
  containerBody: {
    position: 'relative',
    margin: '60px auto',
    padding: 20,
    background: '#fff',
    boxShadow: '2px 4px 20px 0 rgba(0,0,0,0.2)'
  },
  errNoti: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'red',
    textAlign: 'center',
    marginBottom: '20px'
  },
  sendEmailAgain: {
    cursor: 'pointer',
    fontStyle: 'italic',
    fontWeight: 'bold',
    '&:hover': {
      textDecoration: 'underline'
    }
  },
  showLoading: {
    padding: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',

    display: 'none',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingComponent: {
    textAlign: 'center',
    color: '#0677BC'
  },
  emailSent: {
    textAlign: 'center',
    color: '#0677BC',
    padding: '20px',
    background: '#f9f9f9',
    boxShadow: '2px 4px 9px 0 rgba(0,0,0,0.2)',
    transform: 'translateX(-20px)'
  }
});

const SignUpLearner = ({
  rViaSocial,
  rIsSendingEmail,
  rFailedAuth,
  authSocial,
  isSigningUp,
  successfulResponse,
  failedResponse,
  SignUp,
  updateProfileSocial
}) => {
  const classes = useStyles();
  const [usernameU, setUsernameU] = React.useState('');
  const [passwordU, setPasswordU] = React.useState('');
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [inValidEmail, setInValidEmail] = React.useState(false);
  const [email, setEmail] = React.useState('');
  const [displayName, setDisplayName] = React.useState('');
  const [role, setRole] = React.useState('tutor');
  const [social, setSocial] = React.useState('');
  const [isValid, setIsValid] = React.useState(false);

  const handleChangeUsernameU = e => {
    isValid ? setIsValid(!isValid) : '';
    setUsernameU(e.target.value);
  };
  const handleChangePasswordU = e => {
    isValid ? setIsValid(!isValid) : '';
    setPasswordU(e.target.value);
  };
  const handleChangeUsername = e => {
    isValid ? setIsValid(!isValid) : '';
    setUsername(e.target.value);
  };
  const handleChangePassword = e => {
    isValid ? setIsValid(!isValid) : '';
    setPassword(e.target.value);
  };
  const handleChangeEmail = e => {
    isValid ? setIsValid(!isValid) : '';
    setInValidEmail(false);
    setEmail(e.target.value);
  };
  const handleChangeDisplayName = e => {
    isValid ? setIsValid(!isValid) : '';
    setDisplayName(e.target.value);
  };
  const loginViaGoogle = () => {
    setIsValid(false);
    authSocial('google');
    setSocial('google');
  };
  const loginViaFacebook = () => {
    setIsValid(false);
    authSocial('facebook');
    setSocial('facebook');
  };

  const handleSubmitU = e => {
    e.preventDefault();

    if (isEmpty(usernameU) || isEmpty(passwordU)) {
      setIsValid(!isValid);

      return;
    } else {
      const newUser = {
        ...rViaSocial.info,
        username: usernameU,
        password: passwordU,
        role
      };
      updateProfileSocial(social, newUser);
    }
  };
  const validateEmail = email => {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(String(email).toLowerCase());
  };
  const handleSubmit = e => {
    e.preventDefault();
    if (
      isEmpty(username) ||
      isEmpty(password) ||
      isEmpty(email) ||
      isEmpty(displayName)
    ) {
      setIsValid(!isValid);
      return;
    } else if (!validateEmail(email)) {
      setInValidEmail(!validateEmail(email));
    } else {
      const data = { username, password, email, displayName, role };
      SignUp(data);
    }
  };

  return (
    <UserLayout pageTitle="Đăng ký gia sư">
      <Container maxWidth="sm" className={classes.containerBody}>
        <H1 style={{ margin: '20px 0', textTransform: 'uppercase' }}>
          {rViaSocial.isExists === false && rViaSocial.info
            ? 'Đăng ký thông tin đăng nhập'
            : 'Đăng kí tài khoản người dạy'}
        </H1>

        {failedResponse.status || rFailedAuth.status ? (
          <div className={classes.errNoti}>
            <WarningIcon />
            <Box>{failedResponse.message || rFailedAuth.message}</Box>
          </div>
        ) : (
          ''
        )}

        <Container
          className={classes.showLoading}
          style={
            successfulResponse.status ? { zIndex: 1, display: 'flex' } : {}
          }
        >
          <Fade in={successfulResponse.status}>
            <Box className={classes.emailSent}>
              Kiểm tra email đã đăng kí với{' '}
              <strong style={{ fontStyle: 'italic' }}>{username}</strong> để xác
              thực tài khoản!
              <Link href="/sign-in">
                <Button
                  variant="contained"
                  color="primary"
                  style={{ maxWidth: '100px', marginTop: '20px' }}
                >
                  Đóng
                </Button>
              </Link>
            </Box>
          </Fade>
        </Container>
        <Container
          className={classes.showLoading}
          style={isSigningUp ? { zIndex: 1, display: 'flex' } : {}}
        >
          <Fade in={isSigningUp || rFailedAuth.status}>
            <Box className={classes.loadingComponent}>
              <Typography style={{ fontWeight: '600', fontSize: '18px' }}>
                {isSigningUp ? 'Đang kiểm ra thông tin đăng kí' : ''}
              </Typography>
              <CircularProgress />
            </Box>
          </Fade>
        </Container>
        {rViaSocial.isExists === false && rViaSocial.info ? (
          <form
            onSubmit={handleSubmitU}
            style={isSigningUp ? { opacity: '0.2' } : {}}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  error={isValid}
                  fullWidth
                  onChange={handleChangeUsernameU}
                  label="Tên đăng nhập"
                  variant="outlined"
                  id="usernameU"
                  name="usernameU"
                  helperText={
                    isValid
                      ? 'Tài khoản không được bỏ trống. Mời nhập thông tin'
                      : ''
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangePasswordU}
                  error={isValid}
                  label="Mật khẩu"
                  variant="outlined"
                  id="passwordU"
                  name="passwordU"
                  type="password"
                  helperText={
                    isValid
                      ? 'Mật khẩu không được bỏ trống. Mời nhập thông tin'
                      : ''
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <Button variant="contained" color="primary" type="submit">
                  Đăng ký tài khoản với thông tin {rViaSocial.type}
                </Button>
              </Grid>
            </Grid>
          </form>
        ) : (
          <form
            onSubmit={handleSubmit}
            style={
              isSigningUp || rIsSendingEmail || successfulResponse.status
                ? { opacity: '0.2' }
                : {}
            }
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  error={isValid || rFailedAuth.status}
                  fullWidth
                  onChange={handleChangeUsername}
                  label="Tên đăng nhập"
                  variant="outlined"
                  id="username"
                  name="username"
                  helperText={
                    isValid
                      ? 'Tài khoản không được bỏ trống. Mời nhập thông tin'
                      : ''
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangePassword}
                  error={isValid || rFailedAuth.status}
                  label="Mật khẩu"
                  variant="outlined"
                  id="password"
                  name="password"
                  type="password"
                  helperText={
                    isValid
                      ? 'Mật khẩu không được bỏ trống. Mời nhập thông tin'
                      : ''
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangeEmail}
                  error={isValid || rFailedAuth.status || inValidEmail}
                  label="Email"
                  variant="outlined"
                  id="email"
                  name="email"
                  type="text"
                  helperText={
                    isValid
                      ? 'Email không được bỏ trống. Mời nhập thông tin'
                      : inValidEmail
                      ? 'Email không đúng định dạng. Ví dụ: admin@admin.com'
                      : ''
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleChangeDisplayName}
                  error={isValid || rFailedAuth.status}
                  label="Họ và tên"
                  variant="outlined"
                  id="displayName"
                  name="displayName"
                  type="displayName"
                  helperText={
                    isValid
                      ? 'Họ và tên không được bỏ trống. Mời nhập thông tin'
                      : ''
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <Button variant="contained" color="primary" type="submit">
                  Đăng kí
                </Button>
              </Grid>
              <Grid item xs={12}>
                <Typography
                  variant="body1"
                  gutterBottom
                  color="textSecondary"
                  align="center"
                >
                  hoặc
                  <br /> Đăng kí tài khoản qua
                </Typography>
              </Grid>
              <Grid
                item
                xs={12}
                style={{ display: 'flex' }}
                alignItems="center"
                justify="center"
              >
                <Link style={{ cursor: 'pointer' }} onClick={loginViaGoogle}>
                  <GoogleIcon />
                </Link>
                <Link style={{ cursor: 'pointer' }} onClick={loginViaFacebook}>
                  <FacebookIcon />
                </Link>
              </Grid>
              <Grid
                item
                xs={12}
                justify="center"
                alignItems="center"
                style={{ display: 'flex' }}
              >
                <span style={{ fontStyle: 'italic', marginRight: 10 }}>
                  Bạn đã có tài khoản ?
                </span>
                <Link href="/sign-in" style={{ textAlign: 'center' }}>
                  Đăng nhập
                </Link>
              </Grid>
            </Grid>
          </form>
        )}
      </Container>
    </UserLayout>
  );
};

export default withRouter(
  connect(
    state => ({
      rViaSocial: state.authReducer.viaSocial,
      rSentEmail: state.authReducer.sentEmail,
      rIsSendingEmail: state.authReducer.isSendingEmail,
      rFailedAuth: state.authReducer.failedAuth,
      rIsProcessingAuth: state.authReducer.isProcessingAuth,
      isSigningUp: state.signUpReducer.isSigningUp,
      successfulResponse: state.signUpReducer.successfulResponse,
      failedResponse: state.signUpReducer.failedResponse
    }),
    {
      SignUp: signUp,
      VerifyEmail: sendingEmailVerifyAgain,
      authSocial: authViaSocial,
      updateProfileSocial: updateProfileAfterLoginSocial
    }
  )(SignUpLearner)
);
