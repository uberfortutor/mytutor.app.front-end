import React from 'react';
import { Container, Grid } from '@material-ui/core';
import { withRouter } from 'react-router';
import UserLayout from '@containers/layouts/user-layout';
import { H1, Button } from '@commons/components';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { history } = this.props;

    return (
      <UserLayout pageTitle="Đăng ký">
        <Container
          maxWidth="sm"
          style={{
            textAlign: 'center',
            margin: '60px auto',
            padding: 20,
            background: '#fff',
            boxShadow: '2px 4px 20px 0 rgba(0,0,0,0.2)'
          }}
        >
          <H1 style={{ margin: '20px 0', textTransform: 'uppercase' }}>
            Đăng kí tài khoản
          </H1>

          <Grid container spacing={2} style={{ textAlign: 'center' }}>
            <Grid item xs={12}>
              <span>Bạn muốn đăng kí tài khoản với vài trò ?</span>
            </Grid>
            <Grid item xs={12} style={{ display: 'flex' }}>
              <Button onClick={() => history.push('/sign-up-tutor')}>
                Người dạy
              </Button>
              <Button onClick={() => history.push('/sign-up-learner')}>
                Người học
              </Button>
            </Grid>
          </Grid>
        </Container>
      </UserLayout>
    );
  }
}

export default withRouter(SignUp);
