import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@commons/components';
import { get } from 'lodash';
import { Link } from 'react-router-dom';
import {
  Grid,
  Container,
  Avatar,
  Typography,
  Box,
  Chip,
  TextField,
  Divider
} from '@material-ui/core';
import { connect } from 'react-redux';
import { Rating } from '@material-ui/lab';
import NavigationIcon from '@material-ui/icons/Navigation';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import WorkIcon from '@material-ui/icons/Work';
import { restClient } from '@supports/rest-client';
import Loading from '@commons/components/loading';
import UserComment from './user-comment';

const labels = {
  1: 'Quá tệ',
  2: 'Tệ',
  3: 'Tạm ổn',
  4: 'Tốt',
  5: 'Xuất sắc'
};

const useStyles = makeStyles({
  root: {
    // background:'#e8e8e8'
  },
  stickyRightInfo: {
    position: 'fixed',
    right: 0,
    bottom: 0,
    width: '100%',
    textAlign: 'center',
    maxWidth: '300px',
    zIndex: '2222',
    background: 'red'
  },
  avatarImage: {
    maxWidth: '180px',
    maxHeight: '180px',
    width: '100%',
    height: 'auto',
    margin: 'auto'
  },
  avatarImageFixed: {
    maxWidth: '80px',
    maxHeight: '80px',
    width: '100%',
    height: 'auto',
    margin: 'auto'
  },
  description: {
    overflowWrap: 'break-word',
    marginTop: 5
  },
  headerDetail: {
    background: '#ffffff',
    padding: 0
  },
  containerContent: {
    padding: '40px 20px'
  },
  childHeaderDetail: {
    padding: 10
  },
  avatarHeader: {
    textAlign: 'center'
  },
  nameHeader: {
    fontSize: '24px',
    color: '#0677BC'
  },
  itemRightHeader: {
    display: 'flex',
    color: '#0677BC',
    margin: '10px 0'
  },
  middleHeader: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  itemContract: {
    margin: '10px 0',
    display: 'flex',
    justifyContent: 'center'
  },
  amountContract: {
    fontSize: '30px'
  },
  subItemContract: {
    textAlign: 'center'
  },
  titleContent: {
    padding: '20px 0',
    fontSize: '20px'
  },
  contentDetail: {
    background: '#ffffff',
    padding: '20px 30px'
  },
  contentRatingForm: {
    padding: '20px 30px',
    background: '#ffffff'
  },
  titleItemContentDetail: {
    fontWeight: 'bold'
  },
  RatingForm: {
    marginTop: 20,
    padding: 0
  },
  contentForRate: {
    display: 'flex',
    alignItems: 'center'
  },
  CommentForm: {
    marginTop: 10,
    marginBottom: 10
  },
  textComment: {
    width: '100%'
  },
  btnComment: {
    maxWidth: '150px',
    marginTop: '10px'
  },
  rateTutor: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

function DetailTutor({ role, token, id }) {
  const classes = useStyles();
  const AVARTAR_DEFAULT = '/media/avatar-default.png';
  const [hover, setHover] = React.useState(-1);
  // rating value in comment
  const [rate, setRate] = React.useState(0);
  const [isFetch, setIsFetch] = React.useState(false);
  const [tutorInfo, setTutorInfo] = React.useState(null);
  const [listComment, setListComment] = React.useState([]);
  const [commentPage, setCommentPage] = React.useState(1);
  const [loadingComment, setLoadingComment] = React.useState(false);
  const [sendingComment, setSendingComment] = React.useState(false);
  const [hasMore, setHasMore] = React.useState(true);
  const [comment, setComment] = React.useState('');

  const loadTutorDetail = () => {
    restClient.asyncGet(`/tutors/${id}`).then(res => {
      setTutorInfo(get(res, ['data', 'tutor']));
    });
  };

  const loadEvaluations = () => {
    setLoadingComment(true);

    restClient
      .asyncGet(`/tutors/${id}/evaluations`, { page: commentPage, pageSize: 5 })
      .then(res => {
        setLoadingComment(false);

        const cmts = get(res, ['data', 'evaluations'], []) || [];

        if (cmts.length === 0) {
          setHasMore(false);
        }

        setListComment([...listComment, ...cmts]);
        setCommentPage(commentPage + 1);
      });
  };

  React.useEffect(() => {
    if (isFetch) {
      return;
    } else {
      setIsFetch(!isFetch);
      loadTutorDetail();
      loadEvaluations();
      // const getUrlInfoTutor = axios.get(apiHost + `/tutors/${id}`);
      //list comment
      // const getListComment = axios.get(apiHost + `/tutors/${id}/evaluations`);
      // axios
      //   .all([getUrlInfoTutor, getListComment])
      //   .then(
      //     axios.spread((...responses) => {
      //       const tutorInfoFetched = responses[0].data.tutor;
      //       const commentFetched = responses[1].data;

      //       setTutorInfo(tutorInfoFetched);
      //       setListComment(commentFetched);
      //       console.log(get(tutorInfoFetched, 'skills'));
      //       console.log(commentFetched);
      //     })
      //   )
      //   .catch(err => {});
    }
  });

  const handleChangeComment = e => {
    setComment(e.target.value);
  };

  const handleSubmitComment = () => {
    setSendingComment(true);

    restClient
      .asyncPost(`/tutors/${id}/evaluations`, { rate, content: comment })
      .then(res => {
        setSendingComment(false);
        setRate(0);
        setComment('');

        const resData = get(res, 'data');

        if (resData.status) {
          console.log(resData.evaluation);
          setListComment([resData.evaluation, ...listComment]);
        }
      });
  };

  return (
    <div className={classes.root}>
      <Grid container className={classes.containerContent}>
        {/* info Tutor */}
        <Grid item xs={12}>
          {/* General info: avatar-display name price, address, job,  , contract, rate */}
          <Container className={classes.headerDetail}>
            <Grid container className={classes.childHeaderDetail}>
              <Grid item xs={3} className={classes.avatarHeader}>
                <Avatar
                  alt="Remy Sharp"
                  className={classes.avatarImage}
                  src={get(tutorInfo, 'avatar') || AVARTAR_DEFAULT}
                />
                <Typography className={classes.nameHeader}>
                  {get(tutorInfo, 'displayName')}
                </Typography>
                <Box className={classes.rateTutor}>
                  <Rating
                    name="read-only"
                    value={get(tutorInfo, ['tutor', 'rate'])}
                    readOnly
                    size="small"
                  />
                  ({get(tutorInfo, ['tutor', 'numberOfEvaluation'])} Đánh giá)
                </Box>
              </Grid>
              <Grid item xs={5} className={classes.middleHeader}>
                <Box className={classes.itemRightHeader}>
                  <LocationOnIcon />
                  <Typography>
                    {get(tutorInfo, ['address', 'detail'])}{' '}
                    {get(tutorInfo, ['address', 'ward'])}{' '}
                    {get(tutorInfo, ['address', 'province'])}
                  </Typography>
                </Box>
                <Divider />
                <Box className={classes.itemRightHeader}>
                  <WorkIcon />
                  <Typography>{get(tutorInfo, ['tutor', 'job'])}</Typography>
                </Box>
                <Divider />
                <Box className={classes.itemRightHeader}>
                  <AttachMoneyIcon />
                  <Typography>
                    {get(tutorInfo, ['tutor', 'tuitionPerHour'])}/h
                  </Typography>
                </Box>
                <Divider />
              </Grid>
              <Grid
                item
                xs={4}
                style={{
                  padding: '0 20px 0 30px',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                {role === 'tutor' ? null : (
                  <>
                    <Link
                      to={`/tutors/${id}/new-contract`}
                      style={{ textDecoration: 'none', margin: '5px' }}
                    >
                      <Button color="primary" variant="contained" style={{}}>
                        Đăng kí ngay
                      </Button>
                    </Link>
                  </>
                )}
                <Link style={{ textDecoration: 'none', margin: '5px' }}>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => {}}
                  >
                    Chat now
                  </Button>
                </Link>
              </Grid>
            </Grid>
          </Container>
          {/* Description and skills */}
          <Typography className={classes.titleContent}>
            Thông tin gia sư
          </Typography>
          <Container className={classes.contentDetail}>
            <Box className={classes.itemContentDetail}>
              <Box className={classes.titleItemContentDetail}>
                Mô tả về bản thân
              </Box>
              <Box className={classes.description}>
                {get(tutorInfo, ['tutor', 'description'])}
              </Box>
            </Box>
            <Box
              className={classes.itemContentDetail}
              style={{ marginTop: '10px' }}
            >
              <Box className={classes.titleItemContentDetail}>
                Công việc hiện tại
              </Box>
              <Box className={classes.description}>
                {get(tutorInfo, ['tutor', 'job'])}
              </Box>
            </Box>
            <Box
              className={classes.itemContentDetail}
              style={{ marginTop: '10px' }}
            >
              <Box className={classes.titleItemContentDetail}>
                Kĩ năng của bản thân
              </Box>
              <Box className={classes.description}>
                {get(tutorInfo, 'skills', []).map(skill => {
                  return (
                    <Chip
                      label={skill.title}
                      variant="outlined"
                      color="primary"
                      style={{ margin: '5px' }}
                    />
                  );
                })}
              </Box>
            </Box>
          </Container>
          {/* Rating */}
          <Container className={classes.RatingForm}>
            <Typography className={classes.titleContent}>
              Đánh giá gia sư
            </Typography>
            {/* form rate */}
            <Container className={classes.contentRatingForm}>
              {!token || role !== 'finder' ? null : (
                <>
                  <Typography>Bạn cảm thấy gia sư này như thế nào ?</Typography>
                  <Box className={classes.contentForRate}>
                    <Rating
                      name="hover-side"
                      value={rate}
                      onChange={(e, value) => {
                        setRate(value);
                      }}
                      onChangeActive={(event, newHover) => {
                        setHover(newHover);
                      }}
                    />
                    <Box ml={2} style={{ fontSize: '14' }}>
                      {labels[hover !== -1 ? hover : rate]}
                    </Box>
                  </Box>
                  <Container
                    style={{ padding: 0 }}
                    className={classes.CommentForm}
                  >
                    <TextField
                      id="comment"
                      label="Bình luận của bạn"
                      name="comment"
                      onChange={handleChangeComment}
                      multiline
                      className={classes.textComment}
                      rows="2"
                      type="text"
                      variant="outlined"
                      disabled={sendingComment}
                    />
                    <Button
                      color="primary"
                      variant="contained"
                      disabled={sendingComment}
                      className={classes.btnComment}
                      onClick={handleSubmitComment}
                    >
                      <NavigationIcon className={classes.extendedIcon} />
                      Gửi bình luận
                    </Button>
                  </Container>
                  <Divider />
                </>
              )}

              {/* User comment */}
              <Container style={{ padding: 0 }}>
                {listComment.map((cmt, index) => (
                  <>
                    {index === 0 ? <Divider key={index} /> : null}
                    <UserComment comment={cmt} key={index} />
                  </>
                ))}
                {loadingComment ? <Loading size={40} /> : null}
                {hasMore ? (
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={loadingComment}
                    onClick={() => {
                      loadEvaluations();
                    }}
                  >
                    Tải thêm
                  </Button>
                ) : null}
              </Container>
            </Container>
          </Container>
        </Grid>
      </Grid>
    </div>
  );
}

export default connect(state => ({
  token: get(state, ['authReducer', 'user', 'token']),
  role: get(state, ['authReducer', 'user', 'metadata', 'role'])
}))(DetailTutor);
