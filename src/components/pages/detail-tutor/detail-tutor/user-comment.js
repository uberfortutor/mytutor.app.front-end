import React, { Component } from 'react';
import { get } from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Paper, Typography, Box, Divider } from '@material-ui/core';
import { Rating } from '@material-ui/lab';

const useStyles = makeStyles({
  containerComment: {
    margin: '10px 0',
    display: 'flex',
    padding: '10px 20px',
    alignItems: 'center',
    boxShadow: 'unset'
  },
  avatarUser: {
    maxWidth: '50px',
    maxHeight: '50px',
    width: '100%',
    height: 'auto'
  },
  commentUser: {
    margin: '0 10px'
  },
  title: {
    fontSize: '20px'
  },
  content: {
    wordBreak: 'break-word',
    fontSize: '14px',
    color: '#bababa'
  }
});
const UserComment = ({ comment }) => {
  const classes = useStyles();
  const AVARTAR_DEFAULT = '/media/avatar-default.png';

  return (
    <Paper className={classes.containerComment}>
      <Avatar
        alt="Remy Sharp"
        className={classes.avatarUser}
        src={get(comment, ['finder', 'avatar']) || AVARTAR_DEFAULT}
      />

      <Box className={classes.commentUser}>
        <Typography className={classes.title}>
          {get(comment, ['finder', 'displayName']) || ''}
        </Typography>
        <Rating
          name="read-only"
          value={get(comment, 'rate', 0) || 0}
          readOnly
          size="small"
        />
        <Typography className={classes.content}>
          {get(comment, 'content', '') || ''}
        </Typography>
      </Box>
    </Paper>
  );
};

export default UserComment;
