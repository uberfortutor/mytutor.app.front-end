import React, { Component } from 'react';
import { get } from 'lodash';
import { getCookie } from '@supports/utils/cookies';
import { Redirect, withRouter } from 'react-router-dom';
import DetailTutor from './detail-tutor';

const DetailTutorC = ({ match }) => {
  const id = get(match, ['params', 'id']);

  return (
    <div>
      {getCookie('isNew') === 'true' ? (
        <Redirect to="/manager-info/profile-update" />
      ) : (
        <DetailTutor id={id} />
      )}
    </div>
  );
};

export default withRouter(DetailTutorC);
