import React, { Component } from 'react'
import { Chart, ArgumentAxis, ValueAxis, LineSeries,BarSeries } from "@devexpress/dx-react-chart-material-ui";

const Statistic = ()=>{
    
        return (
            <Chart
                data={
                [
                    { argument: 1, value: 10 },
                    { argument: 2, value: 40 },
                    { argument: 3, value: 30 }
                ]}
                >
                <ArgumentAxis />
                <ValueAxis />
                <LineSeries valueField="value" argumentField="argument" />
                <LineSeries valueField="value" argumentField="argument" />
                </Chart>
        )
    
}
export default Statistic