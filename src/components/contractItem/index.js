import React, { Component } from 'react';
import { get, isEmpty, filter } from 'lodash';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import { getCookie } from '@supports/utils/cookies';
import { Chip, Typography } from '@material-ui/core';

const useStyles = makeStyles({
  chipItem: {
    fontWeight: 'bold'
  },
  containerContract: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 30px'
  },
  titleContract: {
    textTransform: 'uppercase',
    fontWeight: 'bold'
  },
  customButton: {
    padding: '8px 0'
  },
  roleIn: {
    margin: '5px 0'
  },
  spaceBottom: {
    marginBottom: '10px',
    '&:hover': {
      transition: 'all 1s',
      boxShadow: '2px 4px 10px 0 rgba(0,0,0,0.2)'
    }
  }
});

const StatusComponent = ({ status }) => {
  const classes = useStyles();

  switch (status) {
    case 0:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Đang thương lượng"
          style={{ border: '2px solid #FFC108', color: '#FFC108' }}
        />
      );
    case 1:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Đang dạy"
          style={{ border: '2px solid #128496', color: '#128496' }}
        />
      );
    case 2:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Hoàn tất hợp đồng"
          style={{ border: '2px solid #218838', color: '#218838' }}
        />
      );
    case 3:
      return (
        <Chip
          className={classes.chipItem}
          variant="outlined"
          label="Đã huỷ hợp đồng"
          style={{ border: '2px solid #C82333', color: '#C82333' }}
        />
      );

    default:
      return;
  }
};

// const role = getCookie('role');

const ContractItem = ({ contract, role }) => {
  const status = 0;
  const classes = useStyles();

  return (
    <Grid item xs={8} className={classes.spaceBottom}>
      <Card className={classes.containerContract}>
        <div>
          <span className={classes.titleContract}>{contract.name}</span>
          <div className={classes.roleIn}>
            {role === 'tutor' ? (
              <span>
                Người học:{' '}
                <i style={{ fontWeight: 'bold' }}>
                  {contract.finder.displayName}
                </i>
              </span>
            ) : (
              <span>
                Người dạy:{' '}
                <i style={{ fontWeight: 'bold' }}>
                  {contract.tutor.displayName}
                </i>
              </span>
            )}
          </div>
          <CardActions className={classes.customButton}>
            <Link
              to={`/contracts/${contract.id}/update`}
              style={{ textDecoration: 'unset' }}
            >
              {contract.status === 0 ? (
                <Button size="small" variant="standard" color="primary">
                  Cập nhật
                </Button>
              ) : (
                ''
              )}
            </Link>
            <Link
              to={`contracts/${contract.id}`}
              style={{ textDecoration: 'unset' }}
            >
              <Button size="small" variant="contained" color="primary">
                Chi tiết
              </Button>
            </Link>
          </CardActions>
        </div>
        <div>
          <StatusComponent status={contract.status} />
        </div>
      </Card>
    </Grid>
  );
};
export default connect(state => ({
  role: get(state, ['authReducer', 'user', 'metadata', 'role'])
}))(ContractItem);
