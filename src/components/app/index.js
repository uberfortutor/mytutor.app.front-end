import React from 'react';
import { withRouter } from 'react-router';
import '@globals/index.css';
import SignUpTuTor from '@containers/pages/signup/forTutor';
import LogIn from '@containers/pages/login/index';
import './App.css';

function App() {
  return <div className="">Welcome to my app !!</div>;
}

export default withRouter(App);
