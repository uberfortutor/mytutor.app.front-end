import React, { Component } from 'react'
import {makeStyles} from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import {Link} from 'react-router-dom'
import NotificationsIcon from '@material-ui/icons/Notifications';
import Popper from '@material-ui/core/Popper';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

const useStyles= makeStyles({
    bell: {
        color: '#0677BC',
        marginRight: '20px',
        cursor: 'pointer'
      },
      popup: {
        
        zIndex:'99999'
      },
      listNoti:{
          background:'#fff',
          boxShadow:'2px 4px 10px 0 rgba(0,0,0,0.3)',
          borderRadius:'5px',
          maxWidth:'350px',
          maxHeight:'450px',
          overflowY:'auto'
      },
})


const ContractListNoti = () => {

    const classes = useStyles();
    
    const [anchorEl,setAnchorEl] = React.useState(null)
    const [open,setOpen] = React.useState(false)
    const handleContractOpen = (e) => {
        e.preventDefault()
        console.log(e.currentTarget)
        setAnchorEl(e.currentTarget);
        setOpen(!open)
        
    }
    const handleClick = () => {
        handleRequestClose()
    }
    const handleRequestClose = () => {
        setOpen(false)
    }
    const handleClickAway = () => {
        // setOpen(!open)
        open === true ? setOpen(false) :''
    }

    return (
        <>
            <ClickAwayListener onClickAway={handleClickAway} >
                <Button className={classes.bell} onClick={handleContractOpen}>
                    <Badge badgeContent={0} color="error">
                        <NotificationsIcon />
                    </Badge>
                </Button>
            </ClickAwayListener>
            <Popper  
                open={open} 
                anchorEl={anchorEl} 
                transition 
                className={classes.popup}
                onRequestClose={handleRequestClose}>
            {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
                <List className={classes.listNoti}>
                    <Link to='/detail-contract' style={{textDecoration:'unset',color:'unset'}}>
                        <ListItem button alignItems="flex-start" onClick={handleClick}>
                        <ListItemAvatar>
                            {/* default source /static/images/avatar/1.jpg - and alt is displayName of sender */}
                            <Avatar alt="hoang viet" src="/static/images/avatar/1.jpg" />
                        </ListItemAvatar>
                        <ListItemText
                        primary="Tên hợp đồng"
                        secondary={
                            <>
                            <Typography
                                component="div"
                                variant="body2"
                                color="textPrimary"
                            >
                                Sender đã cập nhật trạng thái hợp đồng
                            </Typography>
                            <Typography
                                component="div"
                                variant="body2"
                                style={{color:'#d9d9d9',fontSize:'12px',fontStyle:'italic',textAlign:'right'}}
                            >
                                2019/12/25
                            </Typography>
                            
                            </>
                        }
                        />
                    </ListItem>
                    </Link>
                    <Divider variant="inset" component="li" />
                    <Link to='/detail-contract' style={{textDecoration:'unset',color:'unset'}}>
                        <ListItem button alignItems="flex-start" onClick={handleClick}>
                        <ListItemAvatar>
                            {/* default source /static/images/avatar/1.jpg - and alt is displayName of sender */}
                            <Avatar alt="hoang viet" src="/static/images/avatar/1.jpg" />
                        </ListItemAvatar>
                        <ListItemText
                        primary="Tên hợp đồng"
                        secondary={
                            <>
                            <Typography
                                component="div"
                                variant="body2"
                                color="textPrimary"
                            >
                                Sender đã cập nhật trạng thái hợp đồng
                            </Typography>
                            <Typography
                                component="div"
                                variant="body2"
                                style={{color:'#d9d9d9',fontSize:'12px',fontStyle:'italic',textAlign:'right'}}
                            >
                                2019/12/25
                            </Typography>
                            
                            </>
                        }
                        />
                    </ListItem>
                    </Link>
                    <Divider variant="inset" component="li" />
                    {/* <Divider variant="inset" component="li" /> */}
                </List>
            
            </Fade>
            )}
            </Popper>
        
      </>
    )
}

export default ContractListNoti;