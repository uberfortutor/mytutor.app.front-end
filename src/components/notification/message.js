import React, { Component } from 'react'
import {makeStyles} from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import {connect} from 'react-redux';
import {setOpenChatBox} from '@supports/store/redux/actions/main.actions'
import MailIcon from '@material-ui/icons/Mail';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Popper from '@material-ui/core/Popper';
import Fade from '@material-ui/core/Fade';

const useStyles= makeStyles({
    mailbox: {
        color: '#0677BC',
        marginRight: '20px',
        cursor: 'pointer'
      },
      popup: {
        
        zIndex:'99999'
      },
      listNoti:{
          background:'#fff',
          boxShadow:'2px 4px 10px 0 rgba(0,0,0,0.3)',
          borderRadius:'5px',
          maxWidth:'350px',
          maxHeight:'450px',
          overflowY:'auto'
      },
})


const MessageListNoti = ({setOpenChatBox}) => {

    const classes = useStyles();
    const [anchorEl,setAnchorEl] = React.useState(null)
    const [open,setOpen] = React.useState(false)
    const handleMessageOpen = (e) => {
        e.preventDefault()
        console.log(e.currentTarget)
        setAnchorEl(e.currentTarget);
        setOpen(!open)
        
    }
    const handleClick = () => {
        handleRequestClose()
    }
    const handleRequestClose = () => {
        setOpen(false)
        setOpenChatBox()
    }
    const handleClickAway = () => {
        // setOpen(!open)
        open === true ? setOpen(false) :''
    }
    return (
        <>
            <ClickAwayListener onClickAway={handleClickAway} >
                <Button className={classes.mailbox} onClick={handleMessageOpen}>
                    <Badge badgeContent={99} color="error">
                        <MailIcon />
                    </Badge>
                </Button>
            </ClickAwayListener>
            <Popper  
                open={open} 
                anchorEl={anchorEl} 
                transition 
                className={classes.popup}
                onRequestClose={handleRequestClose}>
            {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
                <List className={classes.listNoti}>
                    
                        <ListItem button alignItems="flex-start" onClick={handleClick}>
                        <ListItemAvatar>
                            {/* default source /static/images/avatar/1.jpg - and alt is displayName of sender */}
                            <Avatar alt="hoang viet" src="/static/images/avatar/1.jpg" />
                        </ListItemAvatar>
                        <ListItemText
                        primary="Sender"
                        secondary={
                            <>
                            <Typography
                                component="div"
                                variant="body2"
                                color="textPrimary"
                            >
                                Nội dụng tin nhắn
                            </Typography>
                            </>
                        }
                        />
                    </ListItem>
                    
                    <Divider variant="inset" component="li" />
                    {/* <Divider variant="inset" component="li" /> */}
                </List>
            
            </Fade>
            )}
            </Popper>
            
        </>
    )
}

export default  connect(null,{setOpenChatBox})(MessageListNoti);