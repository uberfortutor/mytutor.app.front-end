import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import { Rating } from '@material-ui/lab';
import { get } from 'lodash';
import { Link } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { ImageLevel } from '@commons/components';

const useStyles = makeStyles({
  card: {
    maxWidth: 300,
    margin: 'auto',
    '&:hover': {
      boxShadow: '2px 4px 10px 0 rgba(0,0,0,0.3)'
    }
  },
  titleTutor: {
    padding: 0,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  rateTutor: {
    padding: 0,
    display: 'flex',
    alignItems: 'center',
    color: '#b3b3b3',
    marginBottom: '10px'
  },
  skills: {
    marginBottom: '10px'
  },
  description: {
    color: '#b3b3b3',
    fontStyle: 'italic'
  },
  customDescription: {
    maxHeight: '60px',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap'
  },
  badgePrice: {
    background: '#fc8225',
    color: 'white',
    fontWeight: 'bold',
    padding: '5px 10px',
    borderRadius: 10,
    fontStyle: 'italic'
  }
});
const AVARTAR_DEFAULT = '/media/avatar-default.png';
export default function TutorComponent({ top, tutorInfo }) {
  const classes = useStyles();
  console.log('top', top);
  return (
    <Card className={classes.card}>
      {/* // style={top === '1'? {}:top === '2'?{marginTop:'10px'}:top==='3'?{marginTop:'20px'}: {}} */}
      <CardActionArea>
        <ImageLevel top={top} />
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="170"
          image={get(tutorInfo, 'avatar') || AVARTAR_DEFAULT}
          title="Contemplative Reptile"
          style={{ filter: 'brightness(0.8)' }}
        />
        <CardContent>
          <Box className={classes.titleTutor}>
            <Typography
              gutterBottom
              variant="h5"
              style={{ fontSize: '18px' }}
              component="h2"
            >
              {get(tutorInfo, 'displayName') || 'Họ tên'}
            </Typography>
            <Typography gutterBottom variant="body2" component="p">
              {get(tutorInfo, ['tutor', 'job']) || 'Nghề nghiệp'}
            </Typography>
          </Box>
          <Box className={classes.rateTutor}>
            <Rating
              name="read-only"
              value={get(tutorInfo, ['tutor', 'rate']) || 0}
              readOnly
              size="small"
              style={{ marginRight: '10px' }}
            />
            <Typography variant="body2" component="span">
              {get(tutorInfo, ['tutor', 'rate']) || 0}/5
            </Typography>
          </Box>

          {/* <Box className={classes.skills}>
            {get(tutorInfo, 'skills', []).map(val => {
              return (
                <Chip
                  label={val.title}
                  variant="outlined"
                  color="primary"
                  style={{ margin: '5px' }}
                />
              );
            })}
          </Box> */}
          <Box className={classes.description}>
            <Tooltip
              TransitionComponent={Zoom}
              title={get(tutorInfo, ['tutor', 'description']) || ''}
              interactive
              placement="right"
              arrow
              style={{ fontSize: '12px' }}
            >
              <Typography
                variant="body2"
                color="inherit"
                component="p"
                className={classes.customDescription}
              >
                {get(tutorInfo, ['tutor', 'description']) || ''}
              </Typography>
            </Tooltip>
          </Box>
        </CardContent>
      </CardActionArea>
      <CardActions
        style={{ justifyContent: 'space-between', alignItems: 'center' }}
      >
        <Box>
          <Chip
            label={`${(get(tutorInfo, ['tutor', 'tuitionPerHour']) || 0) /
              1000}k/giờ`}
            color="primary"
            icon={<QueryBuilderIcon />}
            style={{ fontWeight: 'bold', fontStyle: 'italic' }}
          />
        </Box>
        <Box style={{ display: 'flex' }}>
          <Link
            to={`/tutors/${get(tutorInfo, 'id')}/new-contract`}
            style={{ textDecoration: 'unset' }}
          >
            <Button size="small" color="primary">
              Đăng kí
            </Button>
          </Link>

          <Link
            to={`/tutors/${get(tutorInfo, 'id')}`}
            style={{ textDecoration: 'unset' }}
          >
            <Button
              size="small"
              color="primary"
              style={{ border: '1px solid', marginLeft: 5 }}
            >
              <InfoOutlinedIcon style={{ width: 20, paddingRight: 5 }} /> Chi
              tiết
            </Button>
          </Link>
        </Box>
      </CardActions>
    </Card>
  );
}
