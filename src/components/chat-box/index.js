import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { setCloseChatBox } from '@supports/store/redux/actions/main.actions';
import Fab from '@material-ui/core/Fab';
import CloseIcon from '@material-ui/icons/Close';
import { Widget, addResponseMessage } from 'react-chat-widget';
import './lib/styles.css';
import './index.css';

const ChatBox = ({ isOpenChatBox, setCloseChatBox }) => {
  React.useEffect(() => {
    // addResponseMessage('demo')
  });

  const handleNewUserMessage = newMessage => {
    console.log(`New message incoming! ${newMessage}`);
    // Now send the message throught the backend API
    addResponseMessage('demo');
  };
  const handleCloseChatBox = e => {
    setCloseChatBox();
  };

  return (
    <div>
      {isOpenChatBox ? (
        <>
          <Widget
            handleNewUserMessage={handleNewUserMessage}
            showCloseButton={true}
            // launcher={isOpenChatBox}
            senderPlaceHolder="Nhập tin nhắn"
            subtitle=""
            title="Reciver User"
          />
          {/* <div className="containerButton">
            <Fab
              color="primary"
              style={{ width: 40, height: 40 }}
              onClick={handleCloseChatBox}
            >
              <CloseIcon />
            </Fab>
          </div> */}
        </>
      ) : (
        ''
      )}
    </div>
  );
};

export default connect(
  state => ({
    isOpenChatBox: state.mainReducer.openChatBox
  }),
  { setCloseChatBox }
)(ChatBox);
