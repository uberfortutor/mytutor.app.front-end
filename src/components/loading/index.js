import React from 'react';
import { CircularProgress } from '@material-ui/core';
// import { styles } from '@commons/globals/common-styles';

export default function Loading({ containerStyle, ...props }) {
  return (
    <div
      style={{
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: '0px',
        left: '0px',
        background: 'rgba(6, 119, 188, 0.3)',
        ...containerStyle
      }}
    >
      {/* <CircularProgress style={{ color: '#0677BC' }} {...(props || {})} /> */}
      <CircularProgress {...props} />
    </div>
  );
}
