import { getCookie, updateCookie } from '@supports/utils/cookies';

const headersConfig = {
  headers: {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + getCookie('token')
  }
};
export default headersConfig;
