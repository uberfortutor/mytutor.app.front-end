import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { alreadyAuth } from '@supports/hoc';
import Loadable from 'react-loadable';
import Loading from '@components/loading';

const Page = Loadable({
  loader: () => import('@components/pages'),
  loading: Loading
});

function Routers() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={alreadyAuth(Page)} />
      </Switch>
    </Router>
  );
}

export default Routers;
