import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core/styles';
import { getStore } from '@supports/store';
import theme from '@globals/themes/index';
import Routers from '@containers/routers';

export default function AppContainer() {
  const instance = getStore();

  return (
    <Provider store={instance}>
      <ThemeProvider theme={theme}>
        <Routers />
      </ThemeProvider>
    </Provider>
  );
}
