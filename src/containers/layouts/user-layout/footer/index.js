import React from 'react';
import { Container, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  footer: {
    position: 'relative',
    background: '#0677BC',
    width: '100%',
    height: '110px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'start',
    paddingTop: '20px',
    alignItems: 'center',
    alignContent: 'center'
  },
  logo: {
    width: '39px',
    height: '31px',
    marginBottom: '20px'
  },
  links: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  link: {
    color: '#fff',
    textDecoration: 'none',
    fontSize: '14px',
    fontWeight: 'bold',
    '&:hover': {
      textDecoration: 'underline'
    }
  },
  copyrights: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
    fontSize: '14px',
    color: '#fff',
    fontStyle: 'italic',
    position: 'absolute',
    bottom: '20px'
  }
}));

export default function Footer() {
  const classes = useStyles();

  return (
    <Container className={classes.footer}>
      <Container className={classes.links}>
        <a className={classes.link} href="/">
          Về chúng tôi
        </a>
        <a className={classes.link} href="/">
          Giới thiệu
        </a>
        <a className={classes.link} href="/">
          Điều khoản - chính sách
        </a>
        <a className={classes.link} href="/">
          Góp ý
        </a>
        <a className={classes.link} href="/">
          Hỗ trợ
        </a>
      </Container>
      <Container className={classes.copyrights}>
        <div>TPV HCMUS Anonymuous Team @2019. All rights reserved</div>
      </Container>
    </Container>
  );
}
