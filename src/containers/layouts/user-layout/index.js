import React from 'react';
import { Container } from '@material-ui/core';
import ChatBox from '@components/chat-box';
import Header from './header';
import Footer from './footer';

import './user-layout.css';

export default function UserLayout({ children, pageTitle }) {
  let layoutContainer = React.useRef(null);
  const [scrollVal, setScrollVal] = React.useState(0);
  const [trigger, setTrigger] = React.useState(true);

  return (
    <>
      <div
        id="layoutContainer"
        ref={layoutContainer}
        onScroll={() => {
          if (layoutContainer.current.scrollTop > scrollVal) {
            setTrigger(false);
            setScrollVal(layoutContainer.current.scrollTop);
          } else {
            setTrigger(true);
            setScrollVal(layoutContainer.current.scrollTop);
          }
        }}
        style={{
          width: '100%',
          height: '100%',
          overflowY: 'auto',
          overflowX: 'hidden'
        }}
      >
        <Container className="user-layout" style={{ padding: 0 }}>
          <Header trigger={trigger} />
          <Container
            style={{
              width: '100%',
              padding: '0px',
              minHeight: '690px',
              position: 'relative'
            }}
          >
            {children}
          </Container>
          <ChatBox />
          <Footer />
        </Container>
      </div>
    </>
  );
}
