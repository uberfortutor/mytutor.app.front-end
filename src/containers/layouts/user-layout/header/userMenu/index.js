import React from 'react';
import { getCookie, updateCookie, deleteCookie } from '@supports/utils/cookies';
import { isNull, isEmpty, get } from 'lodash';
import Avatar from '@material-ui/core/Avatar';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Loading from '@components/loading';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import WorkIcon from '@material-ui/icons/Work';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PersonIcon from '@material-ui/icons/Person';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import {
  Container,
  Button,
  Grow,
  Paper,
  ClickAwayListener
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import * as authActions from '@supports/store/redux/actions/auth.actions';
import MessageListNoti from '@components/notification/message'
import ContractListNoti from '@components/notification/contract'


const useStyles = makeStyles(theme => ({
  button: {
    fontSize: 14,
    color: '#0677BC'
  },

  menu: {
    '&::after': {
      display: 'block',
      right: '25px',
      content: '""',
      height: 0,
      top: '-10px',
      width: 0,
      position: 'absolute',
      borderLeft: '10px solid transparent',
      borderRight: '10px solid transparent',
      borderBottom: '10px solid #f9f9f9'
    },

    '&::before': {
      display: 'block',
      right: '25px',
      content: '""',
      height: 0,
      top: '-11px',
      width: 0,
      position: 'absolute',
      borderLeft: '11px solid transparent',
      borderRight: '11px solid transparent',
      borderBottom: '11px solid #fff'
    }
  },
  spacingOne: {
    marginRight: 3
  },
  subMenuLink: {
    fontSize: '14px',
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center',
    color: '#415764',
    padding: '0px 20px'
  },
  loginUserMenu: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
  },
  avatar: {
    marginRight: '5px',
    width: '50px',
    height: '50px',
    borderRadius: '50%',
    overflow: 'hidden',
    position: 'relative',
    background: '#fff'
  },
  avatarImage: {
    width: '100%',
    height: '100%'
  },
  userInfo: {
    display: 'flex',
    alignItems: 'center',
    color: '#0677BC',
    fontWeight: '600',
    marginLeft: '5px'
  },
  
  badge: {}
}));

function UserMenu({ logout, failedAuth, user, isGettingProfile, actions }) {
  const AVATAR_DEFAUL = '/media/avatar-default.png';
  const classes = useStyles();
  const [isOpenUserMenu, setOpenUserMenu] = React.useState(false);
  const [isOpenMessage, setOpenMessage] = React.useState(false);
  const userRef = React.useRef(null);
  const messRef = React.useRef(null);

  React.useEffect(() => {
    const currentURl = window.location.href;
    if (currentURl.includes('sign-in') || currentURl.includes('sign-up'))
      return;
    if (logout) return;
    if (isEmpty(getCookie('token'))) {
      return;
    }
    // setTimeout(() => {
    //   if (!get(user, ['profile']) && !isGettingProfile && !failedAuth.status) {
    //     actions.getProfile();
    //   }
    // }, 100);
  });
  const signOutAccount = () => {
    actions.signOut();
  };
  const handleMessageMenu = () => {
    setOpenMessage(!isOpenMessage);
  };

  const handleCloseMessageMenu = event => {
    if (messRef.current && messRef.current.contains(event.target)) {
      return;
    }

    setOpenMessage(false);
  };
  const handleToggleUserMenu = () => {
    setOpenUserMenu(!isOpenUserMenu);
  };

  const handleCloseUserMenu = event => {
    if (userRef.current && userRef.current.contains(event.target)) {
      return;
    }
    setOpenUserMenu(false);
  };

  const handleListKeyDown = event => {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpenUserMenu(false);
    }
  };
  const token = getCookie('token');
  const role = getCookie('role');
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        position: 'absolute',
        right: '10px'
      }}
    >
      {/* disable message box onClick={handleMessageMenu} ref={messRef} */}
      {token ? ( 
        <>
          <MessageListNoti/>
          <ContractListNoti/>
        </>
      ) : (
        <p />
      )}

      {token ? (
        <Container
          className={classes.loginUserMenu}
          onClick={handleToggleUserMenu}
          ref={userRef}
        >
          <div className={classes.avatar}>
            {isGettingProfile ? (
              <Loading />
            ) : (
              <Avatar
                alt="Remy Sharp"
                className={classes.avatarImage}
                src={get(user, ['profile', 'avatar']) || AVATAR_DEFAUL}
              />
            )}
          </div>

          <div className={classes.userInfo} style={{}}>
            <span>
              {get(user, ['profile', 'displayName']) || 'Default name'}
            </span>
            <ArrowDropDownIcon />
          </div>
        </Container>
      ) : (
        <>
          <Button className={classes.button} href="/sign-in">
            Đăng nhập
          </Button>
          <Button
            className={classes.button}
            style={{ border: '1px solid #0677BC' }}
            ref={userRef}
            onClick={handleToggleUserMenu}
          >
            Đăng ký
          </Button>
        </>
      )}

      <Popper
        open={isOpenUserMenu}
        anchorEl={userRef.current}
        role={undefined}
        style={{ marginTop: '10px' }}
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === 'bottom' ? 'center top' : 'center bottom'
            }}
          >
            <Paper style={{ position: 'relative' }} className={classes.menu}>
              {/* disable onClickAway={handleCloseUserMenu} */}
              <ClickAwayListener onClickAway={handleCloseUserMenu}>
                <MenuList
                  autoFocusItem={isOpenUserMenu}
                  id="menu-list-grow"
                  onKeyDown={handleListKeyDown}
                  style={{ padding: '10px 0px' }}
                >
                  {token ? (
                    <div>
                      <MenuItem onClick={handleCloseUserMenu}>
                        <Link
                          to="/me/info/profile"
                          className={classes.subMenuLink}
                        >
                          <PersonIcon className={classes.spacingOne} />
                          Cập nhật thông tin
                        </Link>
                      </MenuItem>
                      <MenuItem onClick={handleCloseUserMenu}>
                        <Link
                          to="/me/info/change-password"
                          className={classes.subMenuLink}
                        >
                          <LockOpenIcon className={classes.spacingOne} />
                          Đổi mật khẩu
                        </Link>
                      </MenuItem>
                      <MenuItem onClick={handleCloseUserMenu}>
                        <a
                          href="javascript:void(0)"
                          onClick={signOutAccount}
                          className={classes.subMenuLink}
                        >
                          <ExitToAppIcon className={classes.spacingOne} />
                          Đăng xuất
                        </a>
                      </MenuItem>
                    </div>
                  ) : (
                    <div>
                      <MenuItem onClick={handleCloseUserMenu}>
                        <Link
                          to="/sign-up-tutor"
                          className={classes.subMenuLink}
                        >
                          <PermIdentityIcon className={classes.spacingOne} />
                          Người dạy
                        </Link>
                      </MenuItem>
                      <MenuItem onClick={handleCloseUserMenu}>
                        <Link
                          to="/sign-up-learner"
                          className={classes.subMenuLink}
                        >
                          <LocalLibraryIcon className={classes.spacingOne} />
                          Người học
                        </Link>
                      </MenuItem>
                    </div>
                  )}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
}

const mapStateToProps = state => ({
  user: get(state, ['authReducer', 'user']),
  isGettingProfile: get(state, ['authReducer', 'isGettingProfile']),
  failedAuth: get(state, ['authReducer', 'failedAuth']),
  logout: get(state, ['authReducer', 'logout'])
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getProfile: authActions.getProfile,
      signOut: authActions.signOut
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(UserMenu);
