import React from 'react';
import { connect } from 'react-redux';
import { isEmpty, get } from 'lodash';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import GroupIcon from '@material-ui/icons/Group';
import HistoryIcon from '@material-ui/icons/History';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import {
  makeStyles,
  Divider,
  Toolbar,
  Grid,
  Typography,
  useScrollTrigger,
  Slide
} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import { Link } from 'react-router-dom';
import UserMenu from './userMenu';

const useStyles = makeStyles(theme => ({
  toolbar: {
    height: '70px',
    background: '#fff',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    boxShadow: '0 2px 10px 0 rgba(0,0,0,0.2)'
  },
  logo: {
    width: '45px',
    height: '40px'
  },
  titlePage: {
    color: '#0677BC',
    fontSize: '36px',
    textDecoration: 'unset'
  },
  navFeature: {
    top: '70px',
    zIndex: '12',
    maxHeight: '40px'
  },
  navHeader: {
    zIndex: '13'
  },
  customLinkFeature: {
    display: 'flex',
    alignItems: 'center',
    color: '#fff',
    textDecoration: 'unset'
  },
  listItemText: {
    fontSize: '12px'
  },
  customDivider: {
    height: '40px',
    width: ' 2px',
    backgroundColor: 'rgba(255,255,255,0.7)'
  }
}));

// props
function HideOnScroll({ children, trigger }) {
  // const { children, window, layoutContainer } = props;
  // console.log('window', window ? window() : 'nono');
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  // const trigger = useScrollTrigger({
  //   // target: window ? window() : undefined,
  //   // target: document
  //   //   ? get(document.getElementById('layoutContaner'), 'offsetTop')
  //   //   : undefined,
  //   target:
  //     layoutContainer && layoutContainer.current ? layoutContainer : undefined,
  //   threshold: 10
  // });

  return (
    <Slide appear={false} direction="down" in={trigger}>
      {children}
    </Slide>
  );
}

// check role to show feature after login app
function Header({ user, trigger }) {
  const classes = useStyles();
  const token = get(user, 'token', null);
  const role = get(user, ['metadata', 'role'], null);

  return (
    <>
      <AppBar position="sticky" className={classes.navHeader}>
        <Toolbar className={classes.toolbar}>
          <Link to="/" className={classes.titlePage}>
            MyTuTor
          </Link>
          <UserMenu />
        </Toolbar>
      </AppBar>
      <HideOnScroll trigger={trigger}>
        <AppBar className={classes.navFeature} style={{ position: 'sticky' }}>
          <Toolbar style={{ minHeight: '40px', alignItems: 'unset' }}>
            <Grid container direction="row" justify="center">
              <ListItem
                style={{ width: 'unset', paddingTop: 0, paddingBottom: 0 }}
              >
                <Link to="/" className={classes.customLinkFeature}>
                  <ListItemIcon
                    style={{ minWidth: 'unset', marginRight: '10px' }}
                  >
                    <HomeIcon style={{ color: 'white' }} />
                  </ListItemIcon>
                  <ListItemText
                    disableTypography
                    primary={
                      <Typography
                        type="body2"
                        style={{ fontSize: '12px', fontWeight: 'bold' }}
                      >
                        Trang chủ
                      </Typography>
                    }
                  />
                </Link>
              </ListItem>

              <ListItem
                style={{ width: 'unset', paddingTop: 0, paddingBottom: 0 }}
              >
                <Link to="/tutors" className={classes.customLinkFeature}>
                  <ListItemIcon
                    style={{ minWidth: 'unset', marginRight: '10px' }}
                  >
                    <GroupIcon style={{ color: 'white' }} />
                  </ListItemIcon>
                  <ListItemText
                    disableTypography
                    primary={
                      <Typography
                        type="body2"
                        style={{ fontSize: '12px', fontWeight: 'bold' }}
                      >
                        Danh sách giáo viên
                      </Typography>
                    }
                  />
                </Link>
              </ListItem>

              {!isEmpty(token) ? (
                <>
                  <Divider
                    orientation="vertical"
                    className={classes.customDivider}
                  />
                  <ListItem
                    style={{
                      width: 'unset',
                      paddingTop: 0,
                      paddingBottom: 0
                    }}
                  >
                    <Link to="/contracts" className={classes.customLinkFeature}>
                      <ListItemIcon
                        style={{ minWidth: 'unset', marginRight: '10px' }}
                      >
                        <HistoryIcon style={{ color: 'white' }} />
                      </ListItemIcon>
                      <ListItemText
                        disableTypography
                        primary={
                          <Typography
                            type="body2"
                            style={{ fontSize: '12px', fontWeight: 'bold' }}
                          >
                            Hợp đồng của tôi
                          </Typography>
                        }
                      />
                    </Link>
                  </ListItem>
                  {role === 'tutor' ? (
                    <>
                      <ListItem
                        style={{
                          width: 'unset',
                          paddingTop: 0,
                          paddingBottom: 0
                        }}
                      >
                        <Link
                          to="/statistic"
                          className={classes.customLinkFeature}
                        >
                          <ListItemIcon
                            style={{ minWidth: 'unset', marginRight: '10px' }}
                          >
                            <EqualizerIcon style={{ color: 'white' }} />
                          </ListItemIcon>
                          <ListItemText
                            disableTypography
                            primary={
                              <Typography
                                type="body2"
                                style={{ fontSize: '12px', fontWeight: 'bold' }}
                              >
                                Thống kê thu nhập
                              </Typography>
                            }
                          />
                        </Link>
                      </ListItem>
                    </>
                  ) : null}
                </>
              ) : null}
            </Grid>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
    </>
  );
}

function mapStateToProps(state) {
  return { user: get(state, ['authReducer', 'user']) };
}

export default connect(mapStateToProps, null)(Header);
