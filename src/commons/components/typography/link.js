import React from 'react';
import { Typography } from '@material-ui/core';

export function Link({ href, children, style,onClick }) {
  return (
    <Typography
      href={href}
      variant="body1"
      onClick={onClick}
      color="primary"
      style={{
        ...style,
        textDecoration: 'none',
        display: 'block',
        
      }}
      component="a"
    >
      {children}
    </Typography>
  );
}
