import React from 'react';

export default function Empty() {
  return (
    <div style={{ width: '100%', textAlign: 'center' }}>
      Không tìm thấy dư liệu
    </div>
  );
}
