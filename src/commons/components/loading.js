import React from 'react';
import { CircularProgress, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  loadingContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '10px'
  },
  loading: {
    color: 'rgba(6, 119, 188)',
    margin: '10px auto'
  }
});

export default function Loading({
  containerClassName,
  containerStyle,
  className,
  ...props
}) {
  const classes = useStyles();

  return (
    <div
      className={`${classes.loadingContainer}  ${containerClassName || ''}`}
      style={containerClassName || {}}
    >
      <CircularProgress
        className={`${classes.loading} ${className || ''}`}
        {...props}
      />
    </div>
  );
}
