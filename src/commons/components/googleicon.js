import React from 'react';

export function GoogleIcon() {
  return (
    
      <img src="/media/googleicon.png" alt="logo" style={{ width: 35 , height: 35, marginRight: 15}} />
    
  );
}
