import React from 'react';

export function FacebookIcon() {
  return (
    
      <img src="/media/facebookicon.png" alt="logo" style={{ width: 30, height:30, marginRight:20 }} />
    
  );
}
