import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  forImage: {
    position: 'absolute',
    zIndex: '1',
    maxWidth: '100px',
    right: 0
  }
});

export const ImageLevel = props => {
  const classes = useStyles();
  const { top } = props;
  switch (top) {
    case 1 || '1':
      return (
        <img
          className={classes.forImage}
          src="/media/gold-medal.svg"
          alt="hinh"
        />
      );
    case 2 || '2':
      return (
        <img
          className={classes.forImage}
          src="/media/silver-medal.svg"
          alt="hinh"
        />
      );
    case 3 || '3':
      return (
        <img
          className={classes.forImage}
          src="/media/bronze-medal.svg"
          alt="hinh"
        />
      );
    default:
      return <></>;
  }
};
