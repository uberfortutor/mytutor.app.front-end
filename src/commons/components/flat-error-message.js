import React from 'react';
import { isEmpty } from 'lodash';
import { makeStyles, Typography } from '@material-ui/core';
import WarningIcon from '@material-ui/icons/Warning';

const useStyles = makeStyles({
  flatErrorMessage: {
    display: 'flex',
    color: 'red',
    justifyContent: 'center',
    marginBottom: '10px'
  }
});

export default function FlatErrorMessage({ message, className, ...props }) {
  const classes = useStyles();

  return isEmpty(message) ? null : (
    <div
      className={`${classes.flatErrorMessage} ${className || ''}`}
      {...props}
    >
      <WarningIcon />
      <Typography>{message}</Typography>
    </div>
  );
}
