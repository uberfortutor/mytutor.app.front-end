import React from 'react';
import MUIButton from '@material-ui/core/Button';

export function Button({
  variant,
  children,
  disabled,
  type,
  color,
  onClick,
  onChange,
  border,
  loginViaType,
  googlebtn,
  style,
  className
}) {
  return (
    <MUIButton
      onClick={onClick}
      onChange={onChange}
      className={className}
      style={{
        // justifyContent: loginViaType = 'social' ? 'start' : '',
        ...style,
        fontWeight: '600',
        border,
        color:
          googlebtn === 'google'
            ? '#FF8585'
            : variant === 'contained'
            ? 'white'
            : '',
        boxShadow:
          variant === 'contained'
            ? '0px 4px 10px rgba(16, 156, 241, 0.24)'
            : '',
        padding: 10,
        textDecoration: 'none'
      }}
      fullWidth
      variant={variant}
      color={color}
      type={type}
      disabled={disabled}
    >
      {children}
    </MUIButton>
  );
}
