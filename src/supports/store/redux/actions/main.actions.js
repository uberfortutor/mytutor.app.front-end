import axios from 'axios';
import apiHost from '@globals/apiHost/index';
import { getCookie, updateCookie, deleteCookie } from '@supports/utils/cookies';
import {restClient} from '@supports/rest-client'
import * as actionTypes from '../action-types';


const configHeader = {
  headers: {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + getCookie('token')
  }
};
export const gettingSkill = () => ({
  type: actionTypes.GET_SKILL,
  payload: {}
});
export const getSkillSuccess = skills => ({
  type: actionTypes.GET_SKILL_SUCCESS,
  payload: { skills }
});

export const getSkillFail = message => ({
  type: actionTypes.GET_SKILL_FAIL,
  payload: { message }
});
export const getSkills = () => {
  return dispatch => {
    dispatch(gettingSkill());

    const urlSkill = apiHost + '/skills';
    axios
      .get(urlSkill, configHeader)
      .then(res => {
        const { skills } = res.data;
        if (skills) {
          return dispatch(getSkillSuccess(skills));
        } else {
          return dispatch(getSkillFail('Lấy danh sách kỹ năng thất bại'));
        }
      })
      .catch(err => {
        console.log(err.response);
        return dispatch(getSkillFail('Lỗi từ Server'));
      });
  };
};

export const gettingListTutor = () => ({
  type: actionTypes.GETTING_LIST_TUTOR,
  payload: {}
});
export const gettingListTutorSuccess = listTutor => ({
  type: actionTypes.GETTING_LIST_TUTOR_SUCCESS,
  payload: { listTutor }
});
export const gettingListTutorFail = message => ({
  type: actionTypes.GETTING_LIST_TUTOR_FAIL,
  payload: { message }
});
// 1-20 // 20 - 40
// data =  {page: 1, pageSize: 20}
  export const GetListTutor = data => {
  return dispatch => {
    dispatch(gettingListTutor());
    const urlHost = apiHost + '/tutors';
    axios(urlHost, data, configHeader)
      .then(res => {
        const { tutors } = res.data;
        if (tutors) dispatch(gettingListTutorSuccess(tutors));
        else dispatch(gettingListTutorFail('Không lấy được danh sách gia sư'));
      })
      .catch(err => {
        dispatch(gettingListTutorFail('Không lấy được danh sách gia sư'));
        console.log(err.response);
      });
  };
};
  export const gettingListContract = () => ({
    type: actionTypes.GETTING_LIST_CONTRACT,
    payload :{}
  })
  export const gettingListContractSuccess = (listContracts) => ({
    type: actionTypes.GETTING_LIST_CONTRACT_SUCCESS,
    payload :{listContracts}
  })
  export const gettingListContractFail = (message) => ({
    type: actionTypes.GETTING_LIST_CONTRACT_SUCCESS,
    payload :{message}
  })
  export const getListContract = () => {
    return dispatch=>{
      dispatch(gettingListContract())
      RestClient.asyncGet('/contracts/me')
        .then(res => {
          
          const contracts = get(res,['data','contracts'])
          console.log(contracts)
          if(contracts) 
            dispatch(gettingListContractSuccess(contracts))
          else dispatch (gettingListContractFail('Failed'))
        })
        .catch(err => {
          console.log(err)
          dispatch (gettingListContractFail('Failed'))
        })
    }
  }
  
  export const openChatBox = () => ({
    type: actionTypes.OPEN_CHAT_BOX,
    payload:{}
  })
  export const closeChatBox = () => ({
    type: actionTypes.CLOSE_CHAT_BOX,
    payload:{}
  })
  export const setOpenChatBox = () => {
    return dispatch=>dispatch(openChatBox())
  }
  export const setCloseChatBox = () => {
    return dispatch=>dispatch(closeChatBox())
  }