import axios from 'axios';
import apiHost from '@globals/apiHost/index';
import * as actionTypes from '../action-types';

export const isSigningUp = value => ({
  type: actionTypes.IS_SIGNING_UP,
  payload: value
});

export const signUpSuccess = message => ({
  type: actionTypes.SIGN_UP_SUCCESS,
  payload: { message }
});

export const signUpFailed = message => ({
  type: actionTypes.SIGN_UP_FAILED,
  payload: { message }
});

export const signUp = data => {
  return dispatch => {
    dispatch(isSigningUp(true));
    const urlSignUpTuTor = apiHost + '/auth/sign-up';
    axios
      .post(urlSignUpTuTor, data)
      .then(res => {
        dispatch(isSigningUp(false));
        console.log(res.data);
        const { status, message } = res.data;
        if (status) {
          dispatch(signUpSuccess(message));
        } else
          dispatch(
            signUpFailed(
              'Tên đăng nhập đã tồn tại. Mời chọn Tên Đăng nhập khác'
            )
          );
      })
      .catch(err => {
        console.log('err sign up', err);
        dispatch(isSigningUp(false));
        dispatch(signUpFailed('Đã có lỗi xảy ra. Mời thử lại!'));
      });
  };
};
