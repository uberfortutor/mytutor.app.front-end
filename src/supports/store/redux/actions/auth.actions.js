import { getCookie, updateCookie } from '@supports/utils/cookies';
import { get, isEmpty } from 'lodash';
import axios from 'axios';
import apiHost from '@globals/apiHost/index';
import * as actionTypes from '../action-types';
import {
  removeAuthInfoFromCookie,
  saveAuthInfoToCookie
} from '../../../utils/auth';
import { restClient } from '../../../rest-client';

export const signOutSuccess = () => ({
  type: actionTypes.SIGN_OUT_ACCOUNT,
  payload: {}
});

export function signOut() {
  return dispatch => {
    removeAuthInfoFromCookie();
    dispatch(signOutSuccess());
  };
}

export const processingAuth = () => ({
  type: actionTypes.PROCESSING_AUTH,
  payload: {}
});

export const processingAuthDone = () => ({
  type: actionTypes.PROCESSING_AUTH_DONE,
  payload: {}
});

export const signInSuccess = (metadata, token) => ({
  type: actionTypes.SIGN_IN_SUCCESS,
  payload: { metadata, token }
});

export const signInFailed = (
  isLogined,
  message,
  verifyAgain = false,
  locked = false
) => ({
  type: actionTypes.SIGN_IN_FAILED,
  payload: { isLogined, message, verifyAgain, locked }
});

export const signIn = ({ username, password }) => {
  return dispatch => {
    dispatch(processingAuth());
    // call route api login
    // url api: /sign-in {username,password}
    // const urlSignIn = apiHost + '/auth/sign-in';

    restClient
      .asyncPost('/auth/sign-in', { username, password })
      .then(res => {
        dispatch(processingAuthDone());

        if (get(res, 'error')) {
          return dispatch(signInFailed(res.error.message));
        }

        if (get(res, 'data')) {
          const { token, message, metadata } = get(res, 'data', {});

          if (token) {
            saveAuthInfoToCookie(
              token,
              get(metadata, 'isNew'),
              get(metadata, 'role')
            );

            return dispatch(signInSuccess(metadata, token));
          }

          return dispatch(
            signInFailed(true, 'Tài khoản hoặc mật khẩu không đúng')
          );
        }
      })
      .catch(err => {
        dispatch(processingAuthDone());

        switch (err.response.status) {
          case 401:
            return dispatch(
              signInFailed(
                true,
                'Tài khoản hoặc mật khẩu không đúng. Hãy thử lại'
              )
            );
          case 400:
            return dispatch(
              signInFailed(
                true,
                'Tài khoản hoặc mật khẩu không đúng. Hãy thử lại'
              )
            );
          case 403:
            if (err.response.data.unauthorizatedEmail) {
              return dispatch(
                signInFailed(
                  true,
                  'Tài khoản chưa xác thực email. Hãy kiểm tra email để xác thực tài khoản! ',
                  true
                )
              );
            } else {
              return dispatch(
                signInFailed(true, 'Tài khoản của bạn đã bị khoá !', null, true)
              );
            }
        }
        return dispatch(
          signInFailed(true, 'Tài khoản hoặc mật khẩu không đúng! Hãy thử lại')
        );
      });
  };
};

// sending email verify again
export const sendingEmail = isSending => ({
  type: actionTypes.SENDING_EMAIL,
  payload: { isSending }
});

export const sendingEmailDone = isSending => ({
  type: actionTypes.SENDING_EMAIL_DONE,
  payload: { isSending }
});

export const sendingEmailSuccess = (status, message) => ({
  type: actionTypes.SENDING_EMAIL_SUCCESS,
  payload: { status, message }
});

export const sendingEmailVerifyAgain = ({ username, password }) => {
  return dispatch => {
    dispatch(sendingEmail(true));
    const urlEmailVerifyAgain = apiHost + '/auth/send-authorization-email';
    axios
      .post(urlEmailVerifyAgain, { username, password })
      .then(res => {
        dispatch(sendingEmailDone(false));

        const { status, message } = res.data;
        return dispatch(sendingEmailSuccess(status, message));
      })
      .catch(err => {
        dispatch(sendingEmailDone(false));
        console.log(err.response);
      });
  };
};
//-----------------------------
export const getProfileSuccess = profile => ({
  type: actionTypes.GET_PROFILE_SUCCESS,
  payload: { profile }
});
export const getProfileFail = message => ({
  type: actionTypes.GET_PROFILE_FAIL,
  payload: { message }
});
export const gettingProfile = () => ({
  type: actionTypes.GET_PROFILE,
  payload: {}
});
export const getProfile = () => async dispatch => {
  dispatch(gettingProfile());

  await restClient
    .asyncGet('/auth/me/profile')
    .then(res => {
      if (isEmpty(get(res, ['data', 'profile']))) {
        return dispatch(signOut());
      }

      dispatch(getProfileSuccess(get(res, ['data', 'profile'], null)));
    })
    .catch(err => {
      dispatch(getProfileFail(''));
    });

  // await await axios
  //   .get(urlGetProfile, headersConfig)
  //   .then(res => {
  //     if (res.data) {
  //       const { profile } = res.data;
  //       dispatch(getProfileSuccess(profile));
  //     } else {
  //       dispatch(getProfileFail(''));
  //     }
  //   })
  //   .catch(err => {
  //     console.log(err.response);
  //     return dispatch(getProfileFail(''));
  //   });
};
// ----------------------
export const gettingTutorInfo = () => ({
  type: actionTypes.GET_TUTOR_INFO,
  payload: {}
});
export const getTutorInfoSuccess = tutorinfo => ({
  type: actionTypes.GET_TUTOR_INFO_SUCCESS,
  payload: { tutorinfo }
});
export const getTutorInfoFail = tutorinfo => ({
  type: actionTypes.GET_TUTOR_INFO_FAIL,
  payload: {}
});
export const getTutorInfo = () => async dispatch => {
  dispatch(gettingTutorInfo());

  await restClient
    .asyncGet('/auth/me/tutor-info')
    .then(res => {
      if (get(res, 'data')) {
        const { tutorInfo = null } = get(res, 'data', {});

        dispatch(getTutorInfoSuccess(tutorInfo));
      } else {
        dispatch(getTutorInfoFail(''));
      }
    })
    .catch(err => {
      return dispatch(getTutorInfoFail(''));
    });
};
// -----------------------------
export const uploadingAvatar = () => ({
  type: actionTypes.UPLOAD_AVATAR,
  payload: {}
});
export const uploadAvatarSuccess = avatar => ({
  type: actionTypes.UPLOAD_AVATAR_SUCCESS,
  payload: { avatar }
});
export const uploadAvatarFail = message => ({
  type: actionTypes.UPLOAD_AVATAR_FAIL,
  payload: { message }
});
export const uploadAvatar = formData => {
  return async (dispatch, getState) => {
    dispatch(uploadingAvatar());
    const token = getCookie('token');
    const urlAvatar = apiHost + '/auth/me/avatar';

    await axios
      .post(urlAvatar, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: 'Bearer ' + token
        }
      })
      .then(res => {
        const { avatar } = res.data;
        console.log(res.data);
        if (avatar) return dispatch(uploadAvatarSuccess(avatar));
        else
          return dispatch(
            uploadAvatarFail('Cập nhật hình đại diện thất bại. Mời thử lại')
          );
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const UpdatingProfile = () => ({
  type: actionTypes.UPDATING_PROFILE,
  payload: {}
});
export const UpdatingProfileSuccess = (profile, message) => ({
  type: actionTypes.UPDATING_PROFILE_SUCCESS,
  payload: { profile, message }
});
export const UpdatingProfileTutorSuccess = (tutorInfo, message) => ({
  type: actionTypes.UPDATING_PROFILE_TUTOR_SUCCESS,
  payload: { tutorInfo, message }
});
export const UpdatingProfileFail = message => ({
  type: actionTypes.UPDATING_PROFILE_FAIL,
  payload: { message }
});

export const updateProfile = data => {
  return (dispatch, getState) => {
    console.log('body', data);
    dispatch(UpdatingProfile());

    restClient
      .asyncPost('/auth/me/profile', data)
      .then(res => {
        if (res.data) {
          console.log(res.data);
          const { message, profile } = res.data;
          updateCookie('isNew', false);
          console.log('update success');
          dispatch(UpdatingProfileSuccess(profile, message));
        } else {
          dispatch(UpdatingProfileFail('Cập nhật thông tin thất bại'));
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
};
export const updateProfileTutor = data => {
  return (dispatch, getState) => {
    console.log('body', data);
    dispatch(UpdatingProfile());
    const token = getCookie('token');
    const urlUpdateProfile = apiHost + '/auth/me/tutor-info';
    axios
      .post(urlUpdateProfile, data, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        }
      })
      .then(res => {
        if (res.data) {
          console.log(res.data);
          const { message, tutorInfo } = res.data;
          console.log(tutorInfo);
          dispatch(UpdatingProfileTutorSuccess(tutorInfo, message));
        } else {
          dispatch(UpdatingProfileFail('Cập nhật thông tin thất bại'));
        }
      })
      .catch(err => {
        console.log(err.response);
      });
  };
};
export const modeEditProfileOn = () => ({
  type: actionTypes.MODE_EDIT_PROFILE_ON,
  payload: {}
});

export const modeEditProfileDone = () => ({
  type: actionTypes.MODE_EDIT_PROFILE_DONE,
  payload: {}
});

export const modeEditOn = () => {
  return dispatch => {
    dispatch(modeEditProfileOn());
  };
};

export const modeEditOff = () => {
  return dispatch => {
    dispatch(modeEditProfileDone());
  };
};
