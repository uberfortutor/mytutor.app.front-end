import { getCookie, updateCookie } from '@supports/utils/cookies';
import { get, isEmpty } from 'lodash';
import axios from 'axios';
import apiHost from '@globals/apiHost/index';
import { PopupCenter } from '@supports/utils/popupCenter';
import {
  signInSuccess,
  processingAuth,
  processingAuthDone
} from './auth.actions';
import * as actionTypes from '../action-types';

// waiting auth google
export const authViaGoogle = (isExists, info) => ({
  type: actionTypes.AUTH_VIA_GOOGLE,
  payload: { isExists, info }
});
export const authViaFacebook = (isExists, info) => ({
  type: actionTypes.AUTH_VIA_FACEBOOK,
  payload: { isExists, info }
});
// update info after login google/facebook
export const updateProfileAfterViaGoogle = () => ({
  type: actionTypes.AUTH_UPDATE_VIA_GOOGLE,
  payload: {}
});
export const updateProfileAfterViaGoogleSuccess = info => ({
  type: actionTypes.AUTH_UPDATE_VIA_GOOGLE_SUCCESS,
  payload: { info }
});
export const updateProfileAfterViaGoogleFailed = message => ({
  type: actionTypes.AUTH_UPDATE_VIA_GOOGLE_FAILED,
  payload: { message }
});
export const updateProfileAfterViaFacebook = info => ({
  type: actionTypes.AUTH_UPDATE_VIA_FACEBOOK,
  payload: { info }
});

export const updateProfileAfterViaFacebookSuccess = info => ({
  type: actionTypes.AUTH_UPDATE_VIA_FACEBOOK_SUCCESS,
  payload: { info }
});
export const updateProfileAfterViaFacebookFailed = message => ({
  type: actionTypes.AUTH_UPDATE_VIA_FACEBOOK_FAILED,
  payload: { message }
});

export const updateProfileAfterLoginSocial = (type, newUser) => {
  return async (dispatch, getState) => {
    dispatch(processingAuth());
    const state = getState();
    const viaSocial = get(state, ['authReducer', 'viaSocial'], {});
    const type = get(viaSocial, 'type');

    if (get(viaSocial, 'exists') || !get(viaSocial, ['info'])) {
      dispatch(processingAuthDone());
      return;
    }

    // dispatch(updateProfileAfterViaGoogle);
    const urlSocial = apiHost + `/auth/${type}/sign-up`;
    await axios
      .post(urlSocial, {
        ...newUser,
        ...(type === 'google'
          ? { googleId: get(viaSocial, ['info', 'googleId']) }
          : { facebookId: get(viaSocial, ['info', 'facebookId']) }),
        displayName: get(viaSocial, ['info', 'displayName']),
        email: !isEmpty(newUser.email)
          ? newUser.email
          : get(viaSocial, ['info', 'email'])
      })
      .then(res => {
        const resData = get(res, 'data');

        if (resData.status) {
          const token = get(resData, ['data', 'token']);
          const metadata = get(resData, ['data', 'metadata']);

          updateCookie('token', token);
          updateCookie('role', get(metadata, 'role'));
          updateCookie('isNew', get(metadata, 'isNew'));
          return dispatch(signInSuccess(metadata, token));
        }

        if (type === 'google') {
          dispatch(updateProfileAfterViaGoogleFailed(get(resData, 'message')));
        } else if (type === 'facebook') {
          dispatch(
            updateProfileAfterViaFacebookFailed(get(resData, 'message'))
          );
        }
      })
      .catch(err => {
        // must process error here
        console.log('after err', err.response);
      });

    dispatch(processingAuthDone());
  };
};

export const authViaSocial = type => {
  return dispatch => {
    dispatch(processingAuth());

    const urlSocial = apiHost + `/auth/${type}`;
    const positionCenter = PopupCenter(500, 500);

    const socialLogin = window.open(
      urlSocial,
      `Đăng nhập ${type}`,
      'scrollbars=yes, width=' +
        get(positionCenter, 'newWidth') +
        ', height=' +
        get(positionCenter, 'newHeight') +
        ', top=' +
        get(positionCenter, 'newTop') +
        ', left=' +
        get(positionCenter, 'newLeft')
    );
    function checkWindow() {
      if (socialLogin && socialLogin.closed) {
        window.clearInterval(intervalID);
        return dispatch(processingAuthDone());
      }
    }
    var intervalID = window.setInterval(checkWindow, 500);

    window.addEventListener('message', e => {
      const eventData = e.data;

      if (eventData.type === 'LOGIN_VIA_SOCIAL') {
        const res = eventData.messageData;

        if (res.exists) {
          const token = get(res, ['data', 'token'], null);
          const metadata = get(res, ['data', 'metadata'], null);

          updateCookie('token', token);
          updateCookie('role', get(metadata, 'role'));
          updateCookie('isNew', get(metadata, 'isNew'));
          return dispatch(signInSuccess(metadata, token));
        }

        return dispatch(
          type === 'google'
            ? authViaGoogle(
                eventData.messageData.exists,
                eventData.messageData.data
              )
            : authViaFacebook(
                eventData.messageData.exists,
                eventData.messageData.data
              )
        );
      }
    });
  };
};
