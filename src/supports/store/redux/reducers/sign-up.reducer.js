import * as actionTypes from '@supports/store/redux/action-types';

const initStates = {
  isSigningUp: false,
  successfulResponse: {
    status:false,
    message: null
  },
  failedResponse: {
    status: false,
    message: null
  }
};

export const signUpReducer = (state = initStates, { type, payload }) => {
  switch (type) {
    case actionTypes.IS_SIGNING_UP:
      return { ...state, isSigningUp: payload };
    case actionTypes.SIGN_UP_SUCCESS:
      return {
        ...state,
        successfulResponse: {
          ...state.successfulResponse,
          status: true,
          message: payload.message          
        },
        
      };
    case actionTypes.SIGN_UP_FAILED:
      return {
        ...state,
        failedResponse: {
          ...state.failedResponse,
          status:true,
          message: payload.message
        },
        
      };
    
    default:
      return { ...state };
  }
};
