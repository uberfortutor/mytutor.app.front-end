import { get } from 'lodash';
import * as actionTypes from '../action-types';

const initStates = {
  isProcessingAuth: false,
  isSendingEmail: null,
  isGettingProfile: null,
  isGettingTutorInfo: false,
  isUpdatingAvatar: false,
  isUpdatingProfile: false,
  logout: false,
  viaSocial: {
    type: null,
    isExists: null,
    info: null
  },
  sentEmail: {
    status: null,
    message: null
  },
  user: {
    token: null,
    profile: null,
    metadata: null,
    tutor: null
  },
  failedAuth: {
    status: null,
    message: null,
    verifyAgain: null,
    locked: null
  }
};

export const authReducer = (state = initStates, { type, payload }) => {
  switch (type) {
    case actionTypes.GET_TUTOR_INFO: {
      return {
        ...state,
        isGettingTutorInfo: true
      };
    }
    case actionTypes.GET_TUTOR_INFO_SUCCESS: {
      return {
        ...state,
        isGettingTutorInfo: false,
        user: {
          ...state.user,
          tutor: payload.tutorinfo
        }
      };
    }
    case actionTypes.GET_TUTOR_INFO_FAIL: {
      return {
        ...state
      };
    }
    // ------------------------
    case actionTypes.SIGN_OUT_ACCOUNT:
      return {
        ...state,
        logout: true,
        isGettingProfile: false,
        user: {
          ...state.user,
          profile: null,
          token: null,
          metadata: null
        }
      };
    case actionTypes.UPDATING_PROFILE_SUCCESS: {
      return {
        ...state,
        isUpdatingProfile: false,
        user: {
          ...state.user,
          profile: payload.profile,
          metadata: {
            ...get(state, ['user', 'metadata'], {}),
            isNew: false
          }
        }
      };
    }
    case actionTypes.UPDATING_PROFILE_TUTOR_SUCCESS: {
      return {
        ...state,
        isUpdatingProfile: false,
        user: {
          ...state.user,
          tutor: payload.tutorInfor
        }
      };
    }
    case actionTypes.UPDATING_PROFILE: {
      return {
        ...state,
        isUpdatingProfile: true
      };
    }
    case actionTypes.UPDATING_PROFILE_FAIL: {
      return {
        ...state,
        isUpdatingProfile: false,
        failedAuth: {
          ...state.failedAuth,
          message: payload.message
        }
      };
    }
    case actionTypes.UPLOAD_AVATAR:
      return {
        ...state,
        isUpdatingAvatar: true
      };
    case actionTypes.UPLOAD_AVATAR_SUCCESS:
      return {
        ...state,
        isUpdatingAvatar: false,
        user: {
          ...state.user,
          profile: {
            ...state.user.profile,
            avatar: payload.avatar
          }
        }
      };
    case actionTypes.UPLOAD_AVATAR_FAIL:
      return {
        ...state,
        isUpdatingAvatar: false,
        failedAuth: {
          ...state.failedAuth,
          message: payload.message
        }
      };

    case actionTypes.AUTH_UPDATE_VIA_GOOGLE:
      return {
        ...state,
        isProcessingAuth: true
      };
    case actionTypes.AUTH_UPDATE_VIA_GOOGLE_FAILED:
      return {
        ...state,
        isProcessingAuth: false,
        failedAuth: {
          ...state.failedAuth,
          status: true,
          message: payload.message
        }
      };
    case actionTypes.AUTH_UPDATE_VIA_GOOGLE_SUCCESS:
      return {
        ...state,
        isProcessingAuth: false,
        user: {
          ...state.user,
          info: payload.info
        }
      };
    case actionTypes.AUTH_VIA_GOOGLE:
      return {
        ...state,
        viaSocial: {
          ...state.viaSocial,
          type: 'google',
          isExists: payload.isExists,
          info: payload.info
        }
      };
    case actionTypes.AUTH_VIA_FACEBOOK:
      return {
        ...state,
        viaSocial: {
          ...state.viaSocial,
          type: 'facebook',
          isExists: payload.isExists,
          info: payload.info
        }
      };
    case actionTypes.AUTH_UPDATE_VIA_FACEBOOK_FAILED:
      return {
        ...state,
        isProcessingAuth: false,
        failedAuth: {
          ...state.failedAuth,
          status: true,
          message: payload.message
        }
      };
    case actionTypes.SENDING_EMAIL:
      return {
        ...state,
        isSendingEmail: payload.isSending
      };
    case actionTypes.SENDING_EMAIL_DONE:
      return {
        ...state,
        isSendingEmail: payload.isSending
      };
    case actionTypes.SENDING_EMAIL_SUCCESS:
      return {
        ...state,
        sentEmail: {
          ...state.sentEmail,
          status: payload.status,
          message: payload.message
        }
      };

    case actionTypes.PROCESSING_AUTH:
      return { ...state, isProcessingAuth: true };
    case actionTypes.PROCESSING_AUTH_DONE:
      return { ...state, isProcessingAuth: false };
    case actionTypes.SIGN_IN_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          token: payload.token,
          metadata: payload.metadata
        }
      };
    case actionTypes.SIGN_IN_FAILED:
      return {
        ...state,
        failedAuth: {
          ...state.failedAuth,
          status: payload.isLogined,
          message: payload.message,
          verifyAgain: payload.verifyAgain,
          locked: payload.locked
        }
      };
    // ----------------------------
    case actionTypes.GET_PROFILE: {
      return {
        ...state,
        isGettingProfile: true
      };
    }
    case actionTypes.GET_PROFILE_FAIL: {
      return {
        ...state,
        isGettingProfile: false,
        failedAuth: {
          ...state.failedAuth,
          status: true,
          message: payload.message
        }
      };
    }
    case actionTypes.GET_PROFILE_SUCCESS:
      return {
        ...state,
        isGettingProfile: false,
        user: {
          ...state.user,
          profile: payload.profile
        }
      };

    default:
      return { ...state };
  }
};
