export { authReducer } from './auth.reducer';
export { signUpReducer } from './sign-up.reducer';
export { mainReducer } from './main.reducer'