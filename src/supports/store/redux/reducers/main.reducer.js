import * as actionTypes from '../action-types';

const initStates = {
  skills: [],
  isGettingSkill: null,
  isGettingTutorList: null,
  isGettingContract: null,
  listContracts:null,
  openChatBox: false,
  // content chat
  chatBoxContent:null,
  messageFail: {
    status: null,
    message: null
  },
  listTutor: null
};

export const mainReducer = (state = initStates, { type, payload }) => {
  switch (type) {
    case actionTypes.OPEN_CHAT_BOX:
      return {
        ...state,
        openChatBox: true
      }
    case actionTypes.CLOSE_CHAT_BOX:
        return {
          ...state,
          openChatBox: false
        }
    case actionTypes.GETTING_LIST_CONTRACT:
      return {
        ...state,
        isGettingContract:true
      }
    case actionTypes.GETTING_LIST_CONTRACT_SUCCESS:
        return {
          ...state,
          isGettingContract:false,
          isGettingContract: payload.listContracts
        }
    case actionTypes.GETTING_LIST_CONTRACT_FAIL:
        return {
          ...state,
          isGettingContract:false,
          messageFail: {
            ...state.messageFail,
            message: payload.message
          }
        }
    case actionTypes.GETTING_LIST_TUTOR:
      return {
        ...state,
        isGettingTutorList: true
      };
    case actionTypes.GETTING_LIST_TUTOR_SUCCESS:
      return {
        ...state,
        isGettingTutorList: false,
        listTutor: payload.listTutor
      };
    case actionTypes.GETTING_LIST_TUTOR_FAIL:
      return {
        ...state,
        isGettingTutorList: false,
        messageFail: {
          status: true,
          message: payload.message
        }
      };
    case actionTypes.GET_SKILL:
      return {
        ...state,
        isGettingSkill: true
      };
    case actionTypes.GET_SKILL_FAIL:
      return {
        ...state,
        isGettingSkill: false,
        messageFail: {
          ...state.messageFail,
          status: true,
          message: payload.message
        }
      };
    case actionTypes.GET_SKILL_SUCCESS:
      return {
        ...state,
        isGettingSkill: false,
        skills: payload.skills
      };
    // ----------------------
    default:
      return { ...state };
  }
};

