import React from 'react';
import { connect } from 'react-redux';
import { getCookie } from '@supports/utils/cookies';
import { Redirect, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import {
  signInSuccess,
  getProfile
} from '@supports/store/redux/actions/auth.actions';
import { get, isEmpty } from 'lodash';
import { havingAuthInfoInCookie, getAuthInfoFromCookie } from '../utils/auth';

export function alreadyAuth(ComposedComponent) {
  console.log('already hoc');
  function WrappedComponent({
    actions,
    token,
    metadata,
    profile,
    isGettingProfile,
    ...props
  }) {
    console.log('already hoc wrapped component');
    if (!(token && metadata)) {
      if (havingAuthInfoInCookie()) {
        const { token, role, isNew } = getAuthInfoFromCookie();
        actions.signInSuccess({ role, isNew }, token);

        if (!isGettingProfile && isEmpty(profile)) {
          actions.getProfile();
        }
      }
    } else if (!isGettingProfile && isEmpty(profile)) {
      actions.getProfile();
    }

    return <ComposedComponent {...props} />;
  }

  const mapStateToProps = state => ({
    token: get(state, ['authReducer', 'user', 'token']),
    metadata: get(state, ['authReducer', 'user', 'metadata']),
    profile: get(state, ['authReducer', 'user', 'profile']),
    isGettingProfile: get(state, ['authReducer', 'isGettingProfile'])
  });

  const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(
      {
        signInSuccess,
        getProfile
      },
      dispatch
    )
  });

  const ConnectedWrappedComponent = connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrappedComponent);

  return ConnectedWrappedComponent;
}

export function shouldAuth(ComposedComponent, destUrl = null) {
  function WrappedComponent({ token, ...props }) {
    console.log('shouldAuth: ', destUrl);
    if (token) {
      return <ComposedComponent {...props} />;
    }

    return <Redirect push to={{ pathname: '/sign-in', state: { destUrl } }} />;
  }

  const mapStateToProps = state => ({
    token: get(state, ['authReducer', 'user', 'token'])
  });

  const ConnectedWrappedComponent = withRouter(
    connect(mapStateToProps, null)(WrappedComponent)
  );

  return ConnectedWrappedComponent;
}

export function withRole(ComposedComponent, role) {
  function WrappedComponent({ userRole, ...props }) {
    if (userRole !== role) {
      return <NotPermission />;
    }

    return <ComposedComponent {...props} />;
  }

  const mapStateToProps = state => ({
    userRole: get(state, ['authReducer', 'user', 'metadata', 'role'])
  });

  const ConnectedWrappedComponent = withRouter(
    connect(mapStateToProps, null)(WrappedComponent)
  );

  return ConnectedWrappedComponent;
}

export function isNotNewUser(ComposedComponent, role) {
  function WrappedComponent({ token, isNew, location, ...props }) {
    if (token && isNew && location.pathname !== '/me/info/profile') {
      console.log('diff path');
      return <Redirect to="/me/info/profile" />;
    }

    return <ComposedComponent {...props} />;
  }

  const mapStateToProps = state => ({
    token: get(state, ['authReducer', 'user', 'token']),
    isNew: get(state, ['authReducer', 'user', 'metadata', 'isNew'])
  });

  const ConnectedWrappedComponent = withRouter(
    connect(mapStateToProps, null)(WrappedComponent)
  );

  return ConnectedWrappedComponent;
}
