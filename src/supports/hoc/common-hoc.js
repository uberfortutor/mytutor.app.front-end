import React from 'react';
import PageTitle from '../../commons/components/page-title';

export function withPageTitle(ComposedComponent, pageTitle) {
  function WrappedComponent({ ...props }) {
    return (
      <>
        <PageTitle title={pageTitle} />
        <ComposedComponent {...props} />
      </>
    );
  }

  return WrappedComponent;
}
