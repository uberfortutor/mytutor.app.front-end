import { isEmpty } from 'lodash';
import { updateCookie, deleteCookie, getCookie } from './cookies';

export const saveAuthInfoToCookie = (token, isNew, role) => {
  updateCookie('token', token);
  updateCookie('isNew', isNew);
  updateCookie('role', role);
};

export const removeAuthInfoFromCookie = (token, isNew, role) => {
  deleteCookie('token');
  deleteCookie('isNew');
  deleteCookie('role');
};

export const getAuthInfoFromCookie = () => {
  return {
    token: getCookie('token'),
    role: getCookie('role'),
    isNew: getCookie('isNew') === 'true' ? true : false
  };
};

export const havingAuthInfoInCookie = () =>
  !isEmpty(getCookie('token')) &&
  !isEmpty(getCookie('isNew')) &&
  !isEmpty(getCookie('role'));
