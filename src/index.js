import React from 'react';
import ReactDOM from 'react-dom';
import AppContainer from '@containers/app-container';
import { restClient } from './supports/rest-client';
// import * as serviceWorker from './serviceWorker';
console.log('BACKEND_HOST', process.env.REACT_APP_BACKEND_HOST);
restClient.setDomain(process.env.REACT_APP_BACKEND_HOST);

ReactDOM.render(<AppContainer />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();s
